
<footer>
	<div class="row">
		<div class="col-md-4"><a target="_blank" href="http://hes-so.ch"><img class="imgfooter" height="50" src="img/HES-SO_logo_fr.jpg" alt="HES-SO" title="HES-SO"></a></div>
		<div class="col-md-4 "><a target="_blank" href="http://eesp.ch"><img class="imgfooter" height="50" src="img/logo_eesp.png" alt="EESP" title="EESP"></a></div>
		<div class="col-md-4 "><a target="_blank" href="http://cyberlearn.hes-so.ch"><img class="imgfooter" height="50" src="img/logo_cyberlearn.png" alt="Cyberlearn" title="Cyberlearn"></a></div>
	</div>
</footer>

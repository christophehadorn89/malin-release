<?php
include 'header.php';

// Lance la session
session_start();

// Contrôle de sécurité de la session, redirige vers le login
if (!isset($_SESSION['pseudo']) && !isset($_SESSION['password']) && !isset($_SESSION['id'])) {
	header('Location: login.php');
}

// Récupération des variables de session
 $pseudo = $_SESSION['pseudo'];
 $idUser = $_SESSION['id'];

$selectButton = '';
$inputAdmin = '';
$newSearch = '';
$deleteSearch = '';

if ($pseudo === 'admin' && $idUser == 1) {
	$selectButton = '<select name="listYear" id="listYear"></select>';
	$selectButton .= '<select name="listStatut" id="listStatut"></select>';
	$selectButton .= '<select name="listUser" id="listUser"> <button class="btn   btn-primary" id="buttonGoUser">Sélectionner</button></select><p></p>';
	$inputAdmin = '<input type="hidden" id="admin" value=1>';
} else {
	$newSearch = '<p id="infoLog">Vos historiques de recherche sont consultables par vos administrateurs. Vous pouvez changer cela dans l\'onglet profil.</p><p>Vous pouvez également lancer une nouvelle recherche</p><button class="btn   btn-success" id="buttonNewResearch"><i class="fa fa-file-o"></i> Nouvelle recherche</button>';
	$inputAdmin = '<input type="hidden" id="admin" value=0>';
	$deleteSearch = '<button class="btn   btn-danger" id="buttonDeleteResearch"><i class="fa fa-exclamation-triangle"></i> Supprimer</button>';
}

// Regénère un id de session pour + de sécurité
session_regenerate_id();
?>

<div class="content">
<div id="tabs">

	<ul id="menuHead">
		<li id="logoTxt">
			MALIN
		</li>
		<a href='logout.php' class="form_classic_title" id="aLogout" title="Se déconnecter"><i class="fa fa-sign-out"></i><span class="menuTxt">Se déconnecter</span></a>

		<li id="liListResearch" title="Recherches effectuées">
			<a href="#tabList"><i class="fa fa-list-ul"></i><span class="menuTxt">Recherches effectuées</span></a>
		</li>
		<li class="tabToHide" title="Recherche en cours">
			<a href="#tabCurrent"><i class="fa fa-search"></i><span class="menuTxt">Recherche en cours</span></a>
		</li>
		<li id="liLogResearch" title="Journal">
			<a href="#tabLog"><i class="fa fa-history"></i><span class="menuTxt">Journal</span></a>
		</li>

		<li title="Profil">
			<a href="#tabProfil"><i class="fa fa-user"></i><span class="menuTxt">Profil</span></a>
		</li>
		<li title="Informations">
			<a href="#tabInfo"><i class="fa fa-info-circle"></i><span class="menuTxt">Informations</span></a>
		</li>

	</ul>
	<div id="tabInfo">
		<p id="textInfo">La recherche documentaire est un élément essentiel de la mise en oeuvre de pratiques basées sur des résultats de recherche. <br><br>MALIN (Moyens d’Aide à la Littératie Informationnelle et Numérique) est un projet visant à développer des outils et des cours en ligne pour soutenir l'acquisition de compétence en recherche documentaire. <br><br>Cet outil de recherche de mots-clés en est le premier élément. Il a été développé conjointement par la filière ergothérapie de la Haute Ecole de Travail Social et de la Santé - EESP, par la filière physiothérapie de la Haute Ecole de Santé de Genève et par le Centre e-learning // Cyberlearn de la Haute École Spécialisée de Suisse Occidentale.</p>
		<p id="copyright"><img height="50" src="img/copyright.png"/></p>
		<p id="copyright"><a target="_blank" href="https://bitbucket.org/christophehadorn89/malin-release/overview">Code source</a></p>
	</div>
	<div id="tabList">
		<fieldset>
			<h1 id="title">MALIN</h1>
			<h4 id="subtitle">Moyens d’Aide à la Littératie Informationnelle et Numérique</h4>
			
			<div id="pDesc">
			<p id="pWelcome">Bonjour et bienvenue, <span id="pseudo"><?php echo $pseudo?></span></p>
			<p>MALIN est un outil d'aide pour la recherche initiale des mots-clés en anglais et l'exploration de la littérature qui les utilisent</p>
			<div id="listResearchInfo">
				<label id="labelList" for="list">Pour commencer, vous avez la possibilité de récupérer une recherche dans la liste suivante </label>
				<div class="formResearchHisto">
					<?php echo $selectButton; ?>
					<select name="list" id="listResearch">
					</select>
					<button class="btn   btn-primary" id="buttonGoResearch"><i class="fa fa-arrow-right"></i> Aller</button>
					<?php echo $deleteSearch; ?>
				</div>
			</div>
				
			</div>
			<div id="newResearch">
				<?php echo $newSearch; ?>
			</div>

		</fieldset>
	</div>
	<div id ="tabCurrent">
		<div id="overlay"></div>
		<div id="panel" class="fullPanel">
			<form>
				<div class="navDivButton">
						<div class="divButton left ">
							<button class="btn   btn-default buttonBackTraduction">
								<i class="fa fa-angle-double-left"></i>
								<span class="btnTxt"> Retour</span>
							</button>
							<button class="btn   btn-default buttonBackResume">
								<i class="fa fa-angle-double-left"></i>
								<span class="btnTxt"> Retour</span>
							</button>
						</div><div class="divButton right">
							<button class="btn btn-default buttonNextConcept">
								<i class="fa fa-angle-double-right"></i><span class="btnTxt"> Suivant</span>
							</button>
							<button class="btn btn-default buttonNextTraduction">
								<i class="fa fa-angle-double-right"></i><span class="btnTxt"> Suivant</span>
							</button>
						</div>
				</div>
				<div id="divSubject">
					<h4 id="labelSubject" for="textareaResearch">1) Entrez le sujet de votre recherche ci-dessous, en une courte phrase</h4>
					<p></p>
					<textarea id="textareaResearch"  rows="3"></textarea>
				</div>
				<hr>
				<div id="divConceptMain">
					<div class="divWrapConcept">
						<label class="labelResearch">Copier/coller les mots/groupes de mots qui forment les principaux concepts de votre recherche</label>
						<p></p>
						<div class="divConcept">
							<label class ="labelConcept">Concept 1</label>
							<br>
							<input class="inputConcept" type="text">
						</div>
						<div class="divConcept">
							<label class ="labelConcept">Concept 2</label><br>
							<input class="inputConcept" type="text">
						</div>
						<div class="divConcept">
							<label class ="labelConcept">Concept 3</label><br>
							<input class="inputConcept" type="text">
						</div>
						<div class="divConcept">
							<label class ="labelConcept">Concept 4</label><br>
							<input class="inputConcept" type="text">
						</div>
					</div>
					<div id="divErrorConcept"></div>
					<p></p>
				</div>

				<div id="divTraductionMain">
					<div class="divWrapTraduction">
						<h4 class="labelResearch">2) Recherchez les traductions en anglais de vos concepts</h4>
						<p></p>
						<div class="divTraduction"></div>
					</div>
					<div id="divErrorTraduction"></div>
					<p></p>
				</div>
				<div id="divResumeMain">
					<div class="divWrapResume">
						<h4 class="labelResearch">3) Lancez une recherche pour vérifier que vos mots-clés sont bien utilisés dans la littérature. Cliquez sur ceux que vous souhaitez tester</h4>
						<div class="divResume">
							<div id="divResumeSubject"></div>
							<div id="DivAnd"><i class="fa fa-minus"></i></div>
							<fieldset class="divExclusionMain">
								<legend class="labelExclure">Mots-clés à exclure</legend>
								<div class="divExclusion"></div>
							</fieldset>
						</div>
						<div id="divWrapSearch">
							<div id ="tableSearch">
								<div id ="subTableSearch">

						<div id="DivRearchResult">
							<span class="DivIconTxt">Lancez votre recherche</span>
							<i class="fa fa-arrow-circle-down"></i>
						</div>
						<div id="divBrowser">
							<a id="linkGoogleScholar" ><img border="0" alt="Google Scholar" height="42" title="Recherchez avec Google Scholar" src="img/scholar.PNG" height="50"></a>
							<p></p>
							<a id="linkPubMed" ><img border="0" alt="PubMed" title="Recherchez avec PubMed"  src="img/pubmed.png"  height="50"></a>
						</div>
							</div></div>
						</div>
						<div id="divErrorLink"></div>
					</div>

					<div id="divLink">
						<div id="divErrorLink"></div>
					</div>
				</div>
				<div class="navDivButton">
					<div class="divButton left ">
						<button class="btn   btn-default buttonBackTraduction">
							<i class="fa fa-angle-double-left"></i>
							<span class="btnTxt"> Retour</span>
						</button>
						<button class="btn   btn-default buttonBackResume">
							<i class="fa fa-angle-double-left"></i>
							<span class="btnTxt"> Retour</span>
						</button>
					</div><div class="divButton right">
						<button class="btn btn-default buttonNextConcept">
							<i class="fa fa-angle-double-right"></i><span class="btnTxt"> Suivant</span>
						</button>
						<button class="btn btn-default buttonNextTraduction">
							<i class="fa fa-angle-double-right"></i><span class="btnTxt"> Suivant</span>
						</button>
					</div>
				</div>
			</form>
		</div>
		<button id="buttonCloseIframe" class="btn btn-danger">X</button>
		<iframe id="iFrame" src="">
			<p>
				Your browser does not support iframes.
			</p>
		</iframe>
	</div>
	<div id="tabLog" class="container">
	</div>

	<div id="tabProfil">
		<div id="contenu">
			<form id="formProfil"  name='formProfil' action='' method='post' border='0' >
				<div class="form_classic_div">
					<span class="form_classic_title"> <label id="labelNameProfil">Pseudo<span class="red">*</span></label> </span>

					<span class="form_classic_input">
						<input id="inputNameProfil" type='text' />
					</span>
				</div>
				<div id="clear"></div>

				<div class="form_classic_div">
					<span class="form_classic_title"> <label id="labelEmailProfil">Email<span class="red">*</span></label> </span>

					<span class="form_classic_input">
						<input id="inputEmailProfil" type='text' />
					</span>
				</div>
				<div id="clear"></div>

				<div class="form_classic_div">
					<span class="form_classic_title"> <label id="labelPasswordProfil">Mot de passe<span class="red">*</span></label> </span>

					<span class="form_classic_input">
						<input id="inputPasswordProfil" type='password' />
					</span>
				</div>
				<div class="form_classic_div">
					<span class="form_classic_title"> <label id="labelStatutProfil">Statut<span class="red">*</span></label> </span>

					<span class="form_classic_input">
						<select id="selectStatutProfil">
							<option value="etudiant">Etudiant</option>
							<option value="professionnel">Professionnel</option>
						</select>
					</span>
				</div>
				<div id="clear"></div>


				<div id="div_log_info" class="form_classic_div">
				
					<span class="form_classic_title">
						Vos historiques de recherches sont visible par vos administrateurs.
					</span>
					<div class="radioGroup">
						<input type="radio"  id="yesRadioLog" name="logRadio" value="1"><label>Oui</label>
						<input type="radio" id="noRadioLog" name="logRadio" value="0"><label>Non</label>
					</div>

				</div>

				<div id="clear"></div>

				<label id="errorProfil" style="color: red"></label>
				<label id="messageSuccessProfil" style="color: green ; display: none">Profil modifié</label>
				<label id="messageErrorProfil" style="color: red ; display : none">Une erreur est survenue</label>

				<div id="clear"></div>

				<div class="form_center_div">
					<input id="updateButtonProfil" class="submitButton btn btn-primary" type='submit' value='Enregistrer' name="save" form="formProfil"/>
				</div>

			</form>
		</div>
	</div>
</div>

<div id="divSticky"></div>
<div id="div_note">
	<p>Zone de rangement pour les notes</p>
</div>
</div>

<input type="hidden" id="idUser" value="<?php echo $idUser; ?>">
<?php echo $inputAdmin; ?>

<script type="text/javascript" src="js/list.js"></script>
<script type="text/javascript"src="js/index.js"></script>
<script type="text/javascript"src="js/current.js"></script>
<script type="text/javascript"src="js/profil.js"></script>
<script type="text/javascript"src="js/log.js"></script>
<script type="text/javascript" src="js/stickies.js"></script>

</body>
<?php
include 'footer.php';
?>
</html>
$(document).ready(function () {
    $('#errorLogin').hide();

    $('#formLogin').on('submit', function () {

        var pseudo = $("#inputNameLogin").val();
        var password = $("#inputPasswordLogin").val();
        $("input").removeClass("mandatory_input");

        if (pseudo === "" || password === "") {
            $("#errorLogin").show().delay(3000).fadeOut();

            if (pseudo === "") {
                $("#inputNameLogin").addClass("mandatory_input");
            }

            if (password === "") {
                $("#labelPasswordLogin").addClass("mandatory_input");
            }

        } else {

            var formData = {
                pseudo: pseudo,
                password: password,
                action: 'login'
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: formData,
                datatype: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.reponse == "ok") {
                        $("input").removeClass("mandatory_input");
                        window.location.href = 'index.php';
                        resetForm();
                    } else {
                        $("#messageErrorLogin").show().delay(3000).fadeOut();
                        $("#messageErrorLogin").text(obj.reponse);
                    }
                }
            });
        }
        return false;
    });
});

function resetForm() {
    $("#labelNameLogin").css('color', 'black');
    var pseudo = $("#inputNameLogin").val("");

    $("#labelPasswordLogin").css('color', 'black');
    var password = $("#inputPasswordLogin").val("");

    $('#errorLogin').hide();
}
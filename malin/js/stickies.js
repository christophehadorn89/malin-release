var STICKIES = (function () {
    var initStickies = function initStickies() {
            $("#divSticky").html("");
            $('.sticky').each(function () {
                $(this).remove();
            });
            $("<div />", {
                html: "<span class='btnTxt'>Ajouter une</span> note",
                class: "add-sticky btn btn-primary ",
                style: "margin: auto 0;line-height: 25px;",
                click: function () {
                    createSticky();
                }
            }).prependTo("#divSticky");

            $('#divSticky .add-sticky').prepend("<i class='fa fa-plus'></i> ");
            initStickies = null;
        },
        openStickies = function openStickies() {
            initStickies && initStickies();

            if (update == true) {
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {idResearch: idResearch, action: "getNote"},
                    datatype: "json",
                    success: function (data) {
                        data_json = JSON.parse(data);
                        for (var i = 0; i < data_json.length; i++) {
                            console.log(data_json[i]);
                            createSticky(data_json[i]);
                        }
                        $(".sticky").each(function (index) {
                            if ($(this).position().top > $("#div_note").position().top) {
                                $(this).find(".sticky-content").css("display", "none");
                                $(this).find(".sticky-content").addClass("reduce");
                                $(this).find(".fa-minus-circle").css("display", "none");
                                $(this).find(".fa-plus-circle").css("display", "block");
                                $(this).css('width', '200');
                            } else {
                                $(this).find(".sticky-content").css("display", "block");
                                $(this).find(".sticky-content").removeClass("reduce");
                                $(this).find(".fa-minus-circle").css("display", "block");
                                $(this).find(".fa-plus-circle").css("display", "none");
                                $(this).css('width', '300');
                            }
                        });
                    }
                });
            }

        },
        createSticky = function createSticky(data) {
            data = data || {id: +new Date().getTime(), top: "200px", left: "200px", text: "Votre texte ici"}

            return $("<div />", {
                "class": "sticky",
                'id': data.id
            })
                .prepend($("<div />", {"class": "sticky-header"})
                    .append($("<span />", {
                        "class": "sticky-status",
                        text: data.title,
                        click: saveSticky
                    }))
                    .append($("<span />", {
                        "class": "close-sticky",
                        html: "<i class='fa fa-trash'></i>",
                        click: function () {
                            deleteSticky($(this).parents(".sticky").attr("id"));
                        }
                    }))
                    .append($("<span />", {
                        "class": "reduce-sticky",
                        html: "<i class='fa fa-minus-circle'></i>",
                        click: function () {
                            reduceSticky($(this).parents(".sticky").attr("id"));
                        }
                    }))
                    .append($("<span />", {
                        "class": "plus-sticky",
                        html: '<i class="fa fa-plus-circle" style="display:none"></i>',
                        click: function () {
                            plusSticky($(this).parents(".sticky").attr("id"));
                        }
                    }))
            )
                .append($("<div />", {
                    html: data.text,
                    contentEditable: true,
                    "class": "sticky-content",
                    keypress: markUnsaved
                }))
                .draggable({
                    handle: "div.sticky-header",
                    stack: ".sticky",
                    start: markUnsaved,
                    stop: saveSticky,
                    scroll: false,
                })
                .css({
                    position: "absolute",
                    "top": data.top,
                    "left": data.left
                })
                .focusout(saveSticky)
                .appendTo(document.body);
        },
        deleteSticky = function deleteSticky(id) {
            note = {
                id: id
            }
            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: {note: note, action: "deleteNote"},
                datatype: "json",
                success: function (data) {
                    $("#" + id).fadeOut(200, function () {
                        $(this).remove();
                    });
                }, error: function () {
                    //sticky.find(".sticky-status").text("Non enregistré");
                }
            });

        },
        saveSticky = function saveSticky() {
            var that = $(this), sticky = (that.hasClass("sticky-status") || that.hasClass("sticky-content")) ? that.parents('div.sticky') : that,
                note = {
                    top: sticky.css("top"),
                    left: sticky.css("left"),
                    text: sticky.children(".sticky-content").html(),
                    id: sticky.attr("id"),
                    title: sticky.find(".sticky-status").text()
                }

            if ($("#div_note").position().top > sticky.position().top) {
                sticky.find(".sticky-content").css("display", "block");
                sticky.find(".sticky-content").removeClass("reduce");
                sticky.find(".fa-minus-circle").css("display", "block");
                sticky.find(".fa-plus-circle").css("display", "none");
                sticky.css('width', '300');
            } else {
                sticky.find(".sticky-content").css("display", "none");
                sticky.find(".sticky-content").addClass("reduce");
                sticky.find(".fa-minus-circle").css("display", "none");
                sticky.find(".fa-plus-circle").css("display", "block");
                sticky.css('width', '200');
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: {note: note, idResearch: idResearch, action: "addNote"},
                datatype: "json",
                success: function (data) {
                    sticky.find(".sticky-status").text(sticky.children(".sticky-content")
                        .contents()
                        .filter(function () {
                            return !!$.trim(this.innerHTML || this.data);
                        })
                        .first().text());
                    //sticky.find(".sticky-status").text("Enregistré");
                }, error: function () {
                    //sticky.find(".sticky-status").text("Non enregistré");
                }
            });

        }, reduceSticky = function reduceSticky(id) {
            var position_div_note = $("#div_note").offset();
            var position_current_note = $("#" + id).offset();

            $("#" + id).offset({top: position_div_note.top + 10, left: position_current_note.left});
            $("#" + id).find(".sticky-content").css("display", "none");
            $("#" + id).find(".sticky-content").addClass("reduce");
            $("#" + id).find(".fa-minus-circle").css("display", "none");
            $("#" + id).find(".fa-plus-circle").css("display", "block");
            $("#" + id).css('width', '200');

            var note = {
                top: $("#" + id).css("top"),
                left: $("#" + id).css("left"),
                text: $("#" + id).children(".sticky-content").html(),
                id: $("#" + id).attr("id"),
                title: $("#" + id).find(".sticky-status").text()
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: {note: note, idResearch: idResearch, action: "addNote"},
                datatype: "json",
                success: function (data) {
                    $("#" + id).find(".sticky-status").text($("#" + id).children(".sticky-content")
                        .contents()
                        .filter(function () {
                            return !!$.trim(this.innerHTML || this.data);
                        })
                        .first().text());
                    //sticky.find(".sticky-status").text("Enregistré");
                }, error: function () {
                    //sticky.find(".sticky-status").text("Non enregistré");
                }
            });
        }, plusSticky = function plusSticky(id) {
            var position_div_note = $("#div_note").offset();
            var position_current_note = $("#" + id).offset();

            $("#" + id).offset({top: "300", left: position_current_note.left});
            $("#" + id).find(".sticky-content").css("display", "block");
            $("#" + id).find(".sticky-content").removeClass("reduce");
            $("#" + id).find(".fa-minus-circle").css("display", "block");
            $("#" + id).find(".fa-plus-circle").css("display", "none");
            $("#" + id).css('width', '300');

            var note = {
                top: $("#" + id).css("top"),
                left: $("#" + id).css("left"),
                text: $("#" + id).children(".sticky-content").html(),
                id: $("#" + id).attr("id"),
                title: $("#" + id).find(".sticky-status").text()
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: {note: note, idResearch: idResearch, action: "addNote"},
                datatype: "json",
                success: function (data) {
                    $("#" + id).find(".sticky-status").text($("#" + id).children(".sticky-content")
                        .contents()
                        .filter(function () {
                            return !!$.trim(this.innerHTML || this.data);
                        })
                        .first().text());
                }, error: function () {
                }
            });
        },
        markUnsaved = function markUnsaved() {

            var that = $(this), sticky = that.hasClass("sticky-content") ? that.parents("div.sticky") : that;
            sticky.find(".sticky-status").text(sticky.children(".sticky-content")
                .contents()
                .filter(function () {
                    return !!$.trim(this.innerHTML || this.data);
                })
                .first().text());
        }
    return {
        open: openStickies,
        init: initStickies,
        "new": createSticky,
        remove: deleteSticky,
        save: saveSticky,
        reduce: reduceSticky,
        plus: plusSticky
    };
}());
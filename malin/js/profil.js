$(function () {

    var dataInitial = {
        idUser: idUser,
        action: 'getUser'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);

            if (obj != null) {
                $("#inputEmailProfil").val(obj.email);
                $("#inputNameProfil").val(obj.name);
                $("#inputPasswordProfil").val(obj.password);
                $("#selectStatutProfil option").filter(function () {
                    //may want to use $.trim in here
                    return $(this).text() == obj.statut;
                }).prop('selected', true);

                if (obj.authorize_log == 0) {
                    $("#noRadioLog").prop("checked", true);
                } else {
                    $("#yesRadioLog").prop("checked", true);
                }

                if (obj.name == 'admin') {
                    $("#inputNameProfil").prop('disabled', true);
                    $("#div_log_info").hide();
                }

            } else {
                $("#messageErrorProfil").show();
            }
        }
    });

    $('#errorProfil').hide();

    $('#formProfil').on('submit', function () {

        $("#messageErrorProfil").hide();

        var pseudoProfil = $.trim($("#inputNameProfil").val());
        var emailProfil = $.trim($("#inputEmailProfil").val());
        var passwordProfil = $.trim($("#inputPasswordProfil").val());
        var authorize_log = $("input[name='logRadio']:checked").val();
        var statutProfil = $("#selectStatutProfil option:selected").text();

        if (pseudoProfil === "" || passwordProfil === "" || emailProfil === "") {
            $("#errorProfil").html("Tous les champs sont obligatoires");
            $("#errorProfil").show().delay(3000).fadeOut();
        } else {

            var formData = {
                pseudo: pseudoProfil,
                idUser: idUser,
                password: passwordProfil,
                statut: statutProfil,
                log: authorize_log,
                email: emailProfil,
                action: 'updateUser'
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: formData,
                datatype: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.reponse == "ok") {
                        $("#messageSuccessProfil").html("Profil modifié avec succès").show().delay(3000).fadeOut();

                        // Add Log for profil
                        //addLog("Profil", "L'utilisateur a modifié son profil");
                    } else {
                        $("#messageErrorProfil").show().delay(3000).fadeOut();
                        $("#messageErrorProfil").text(obj.reponse);
                    }
                }
            });
        }
        return false;
    });
});

$(document).ready(function () {

    $('#errorRegister').hide();

    $('#formRegister').on('submit', function () {

        var emailProfil = $.trim($("#inputEmailRegister").val());
        var pseudo = $.trim($("#inputNameRegister").val());
        var password = $.trim($("#inputPasswordRegister").val());
        var statutProfil = $("#selectStatutRegister option:selected").text();
        $("input").removeClass("mandatory_input");

        if (pseudo === "" || password == "" || emailProfil == "") {

            if (pseudo === "") {
                $("#inputNameRegister").addClass("mandatory_input");
            }

            if (password === "") {
                $("#inputPasswordRegister").addClass("mandatory_input");
            }

            if (emailProfil === "") {
                $("#inputEmailRegister").addClass("mandatory_input");
            }

            $("#errorRegister").html("Tous les champs sont obligatoires");
            $("#errorRegister").show().delay(3000).fadeOut();

        } else {

            var formData = {
                pseudo: pseudo,
                password: password,
                statut: statutProfil,
                email: emailProfil,
                action: 'register'
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: formData,
                datatype: "json",
                success: function (data) {
                    console.log(data);
                    var obj = jQuery.parseJSON(data);
                    if (parseInt(obj.reponse) > 0) {
                        $("input").removeClass("mandatory_input");
                        $("#messageSuccessRegister").html("Utilisateur ajouté avec succès").show().delay(3000).fadeOut();
                        addLogProfil("Utilisateur", "Création du profil", new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'), obj.reponse);
                        resetForm();
                        setTimeout(function () {
                            window.location.href = "index.php";
                        }, 3000);
                    } else {
                        $("#messageErrorRegister").show().delay(3000).fadeOut();
                        $("#messageErrorRegister").text(obj.reponse);
                    }
                }
            });
        }
        return false;
    });
});

function resetForm() {
    $("#labelNameRegister").css('color', 'black');
    var pseudo = $("#inputNameRegister").val("");

    $("#labelPasswordRegister").css('color', 'black');
    var password = $("#inputPasswordRegister").val("");

    $("#labelEmailRegister").css('color', 'black');
    var email = $("#inputEmailRegister").val("");

    $('#error').hide();
}

function addLogProfil(actionLog, textLog, time, idUser) {

    var dataInitial = {
        actionLog: actionLog,
        textLog: textLog,
        idUser: idUser,
        time: time,
        action: 'addLogProfil'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
        }
    });
}
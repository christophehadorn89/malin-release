$(function () {

    $("#tabs").tabs({
        activate: function (event, ui) {
            hideNote();
            hideFooter();
        }
    });

    $('.buttonNextTraduction').click(function (e) {
        e.preventDefault();
        startLoad();
        fillTableKeyWords();
    });

    $('.buttonBackTraduction').click(function (e) {
        e.preventDefault();
        startLoad();
        backToConceptStep();
        addLog($("#textareaResearch").val(), "Etape 2 : Retour à l'étape de l'élaboration des concepts", idResearch, new Date().toString('yyyy-MM-dd HH:mm:ssZ'));
        replaceNote();
    });

    $('.buttonBackResume').click(function (e) {
        e.preventDefault();
        startLoad();
        backToTraductionStep();
        addLog($("#textareaResearch").val(), "Etape 3 : Retour à l'étape de traduction des concepts", idResearch, new Date().toString('yyyy-MM-dd HH:mm:ssZ'));
        replaceNote();
    });

    $('.buttonNextConcept').click(function (e) {
        e.preventDefault();
        startLoad();
        var mandatory = goToTraductionStep();
        if (!mandatory) {
            sendResearchToServerInitial();
        } else {
            replaceNote();
        }
    });

    $("#buttonCloseIframe").click(function (e) {
        e.preventDefault();
        $('#iFrame').hide();
        $(this).hide();
        $("#panel").removeClass('leftPanel');
        $("#panel").addClass('fullPanel');
    });

    $('form').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

});




$(function () {

    $('#liLogResearch').click(function (e) {

        var dataInitial = {
            idUser: idUser,
            action: 'getLog'
        }

        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: dataInitial,
            datatype: "json",
            success: function (data) {

                var myLog = jQuery.parseJSON(data);

                $("#tabLog").html('');

                if (myLog.length > 0) {
                    if (admin == true) {
                        $("#tabLog").append('<div class="row"><div class="col-md-12 center"><form method="GET" action="./log.php" class="exportLog"><button type="submit" class="btn btn-primary exportAllLog">Exporter tous les logs de tous les utilisateurs</button></form></div>');
                    }

                    $("#tabLog").append('<div class="row header"><div class="col-md-1"><h3>Pseudo</h3></div><div class="col-md-3"><h3>Recherche</h3></div><div class="col-md-6"><h3>Texte</h3></div><div class="col-md-2"><h3>Date</h3></div></div>');

                    for (var i = 0; i < myLog.length; i++) {
                        $("#tabLog").append('<div class="row"><div class="col-md-1">' + myLog[i].pseudo + '</div><div class="col-md-3">' + myLog[i].action + '</div><div class="col-md-6">' + myLog[i].text + '</div><div class="col-md-2 last">' + myLog[i].hour + '</div></div>');
                    }

                } else {
                    $("#tabLog").append('<div class="row"><div class="col-md-12 text-center"><h3>Aucun log de disponible</h3></div>');
                }
            }
        });
    });
});

var idUser;
var admin = false;

$(function () {

    idUser = $("#idUser").val();
    admin = $("#admin").val();

    if (admin == false) {
        getResearchList();
    } else {
        getYearList();

        $("#listUser").change(function () {
            idUser = $('#listUser').children(":selected").prop("value");
            getResearchList();
        });
        $("#listYear").change(function () {
            getStatutList();
        });
        $("#listStatut").change(function () {
            getUserList();
        });
    }

    $('#liListResearch').click(function (e) {
        if (admin == false) {
            getResearchList();
        } else {
            getYearList();
        }
    });

});

function getResearchList() {

    var dataInitial = {
        idUser: idUser,
        action: 'getResearchList'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            $("#listResearch").html("");
            if (obj.length != 0) {
                for (var i = 0; i < obj.length; i++) {
                    $("#listResearch").append('<option value="' + obj[i][0] + '">' + obj[i][1] + " - " + obj[i][2] + '</option>');
                    $("#buttonGoResearch").show();
                }
                $("#listResearch option[value=" + idResearch + "]").prop("selected", "selected");
                $("#listResearch").show();
                $("#listResearchInfo").show();
            } else {
                $("#listResearch").hide();
                $("#buttonGoResearch").hide();
                if (admin == false) {
                    $("#listResearchInfo").hide();
                }
            }
        }
    });
}

function getUserList() {

    var dataInitial = {
        action: 'getUserList',
        time: $("#listYear option:selected").text(),
        statut: $("#listStatut option:selected").text()
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            $("#listUser").html("");
            if (obj.length != 0) {
                for (var i = 0; i < obj.length; i++) {
                    $("#listUser").append('<option value="' + obj[i][0] + '">' + obj[i][1] + '</option>');
                }
                idUser = $('#listUser').children(":selected").prop("value");

                getResearchList();
                $("#listUser").show();

            } else {
                $("#listUser").hide();
                $("#listResearch").hide();
                $("#buttonGoResearch").hide();
            }
        }
    });
}

function getYearList() {

    var dataInitial = {
        action: 'getYearList'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            $("#listYear").html("");
            if (obj.length != 0) {
                for (var i = 0; i < obj.length; i++) {
                    $("#listYear").append('<option value="' + obj[i][0] + '">' + obj[i][0] + '</option>');
                }

                getStatutList();
                $("#listStatut").show();

            } else {
                $("#listStatut").hide();
                $("#listUser").hide();
                $("#listResearch").hide();
                $("#buttonGoResearch").hide();
            }
        }
    });
}

function getStatutList() {

    var dataInitial = {
        action: 'getStatutList',
        time: $("#listYear option:selected").text()
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            $("#listStatut").html("");

            if (obj.length != 0) {
                for (var i = 0; i < obj.length; i++) {
                    $("#listStatut").append('<option value="' + obj[i][0] + '">' + obj[i][0] + '</option>');
                }
                getUserList();
                $("#listUser").show();

            } else {
                $("#listStatut").hide();
                $("#listUser").hide();
                $("#listResearch").hide();
                $("#buttonGoResearch").hide();
            }
        }
    });
}
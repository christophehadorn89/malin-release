$(document).ready(function () {
    $('#errorRegister').hide();

    $('#formForget').on('submit', function () {

        var pseudo = $.trim($("#inputPseudoForget").val());
        $("#inputPseudoForget").removeClass("mandatory_input");

        if (pseudo == "") {
            $("#inputPseudoForget").addClass("mandatory_input");
            $("#errorForget").html("Tous les champs sont obligatoires");
            $("#errorForget").show().delay(3000).fadeOut();
        } else {

            var formData = {
                pseudo: pseudo,
                action: 'forget'
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: formData,
                datatype: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data);
                    if (obj.reponse == true) {
                        $("#inputPseudoForget").removeClass("mandatory_input");
                        $("#messageSuccessForget").html("Un nouveau mot de passe vous a été envoyé par email").show().delay(3000).fadeOut();
                        resetForm();
                        setTimeout(function () {
                            window.location.href = "index.php";
                        }, 3000);
                    } else {
                        $("#messageErrorForget").show().delay(3000).fadeOut();
                        $("#messageErrorForget").text(obj.reponse);
                        $("#inputPseudoForget").addClass("mandatory_input");
                    }
                }
            });
        }
        return false;
    });
});

function resetForm() {
    $("#labelPseudoForget").css('color', 'black');
    $("#inputPseudoForget").val("");
    $('#error').hide();
}
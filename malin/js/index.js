var idResearch = 0;
var update = false;
var objResearchUpdate;
var timeNewResearch = "";
var objExclude = [];

$(function () {

    //Hide tab
    $('.tabToHide').hide();

    // Action to click on Delete Research
    $('#buttonDeleteResearch').click(function (e) {

        var result = confirm("Etes-vous sûr ?");
        if (result) {
            var idResearchToDelete = $("#listResearch").children(":selected").prop("value");
            var nameResearch = $("#listResearch").children(":selected").html();
            var nameResearchSub = nameResearch.substr(0, nameResearch.indexOf('-'));

            var data = {
                idResearch: idResearchToDelete,
                action: 'deleteResearch'
            }

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: data,
                datatype: "json",
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    if (data == true) {
                        getResearchList();
                        addLog(nameResearchSub, " a supprimé cette recherche ", idUser, idResearchToDelete, new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'));
                    }
                }
            });
        }
    });

    // Action to click on Go Research
    $('#buttonGoResearch').click(function (e) {

        $(".inputConcept").removeClass("mandatory_input");
        $("#textareaResearch").removeClass("mandatory_input");
        $(".inputTraduction ").removeClass("mandatory_input");
        $("#divErrorConcept").text("");
        $("#divErrorTraduction").text("");
        $("#divErrorLink").text("");

        startLoad();

        //Get id and name of the research in the list
        idResearch = $("#listResearch").children(":selected").prop("value");

        var data = {
            idResearch: idResearch,
            action: 'getResearch'
        }

        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: data,
            datatype: "json",
            success: function (data) {
                objResearchUpdate = jQuery.parseJSON(data);
                update = true;
                fillConceptFormUpdate(objResearchUpdate);

                // View research log
                addLog(objResearchUpdate.name, "Etape 0 : A été voir la recherche " + objResearchUpdate.name, idResearch, new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'));

                $('.tabToHide').show();
                $('#tabs').tabs('option', "active", 1);
                replaceNote();
            }
        });
    });

    if (admin == false) {
        $('#buttonNewResearch').click(function (e) {
            startLoad();
            update = false;
            $('.tabToHide').show();
            $('#tabs').tabs('option', "active", 1);
            $(".inputConcept").val("");
            $('.divTraduction').html("");
            $("#textareaResearch").val("");
            backToConceptStep();
            $("#divResumeMain").hide();
            $(".sticky").remove();
            timeNewResearch = new Date().toString('yyyy-MM-dd HH:mm:ss');
            idResearch = 0;
            objExclude = [];
            replaceNote();
            $(".inputConcept").removeClass("mandatory_input");
            $("#textareaResearch").removeClass("mandatory_input");
            $(".inputTraduction ").removeClass("mandatory_input");
            $("#divErrorConcept").text("");
            $("#divErrorTraduction").text("");
            $("#divErrorLink").text("");
        });
    }

    $(".buttonNextConcept").show();
    $(".buttonNextTraduction").hide();
    $(".buttonBackTraduction").hide();
    $(".buttonBackResume").hide();
});

function fillTradFormNew() {

    var divContainerMainTraduction = "";
    var spanTraduction = "";
    var labelTraduction = "";
    var inputTraduction = "";
    var buttonRemoveTraduction = "";
    var buttonAddTraduction = "";
    var divContainerTraductionB = "";
    var inputTraductionB = "";
    var divContainerTraduction = "";

    // Array research / concept for log
    var arrayResearchConceptLog = new Array()

    //Add research to array for log
    arrayResearchConceptLog.push($("#textareaResearch").val());

    var cptConcept = 1;
    $("#labelSubject").hide();
    $("#divConceptMain").hide();
    $("textarea").prop("disabled", true);

    $(".divTraduction").empty();

    $('.inputConcept').each(function () {

        inputValue = $(this).val();

        if (inputValue.length > 0) {

            //Add concept to array for log
            arrayResearchConceptLog.push(inputValue);

            divContainerMainTraduction = $('<fieldset/>', {
                class: "divContainerMainTraduction"
            });

            divContainerTraduction = $('<div/>', {
                class: "divContainerTraduction"
            });

            spanTraduction = $('<span/>', {
                class: "spanTraduction",
                html: inputValue
            });

            labelTraduction = $('<legend/>', {
                class: "labelTraduction",
                html: "Concept " + cptConcept + " : "
            });

            cptConcept++;
            inputTraduction = $('<input/>', {
                class: "inputTraduction ",
                type: "text"
            });

            buttonAddTraduction = $('<button />', {
                class: "btn btn-success buttonAddTraduction ",
                html: "<i class='fa fa-plus'></i>",
                on: {
                    click: function (e) {

                        e.preventDefault();

                        divContainerTraductionB = $('<div/>', {
                            class: "divContainerTraduction sup"
                        });

                        inputTraductionB = $('<input/>', {
                            class: "inputTraduction ",
                            type: "text"
                        });

                        buttonRemoveTraduction = $('<button />', {
                            class: "btn btn-danger buttonRemoveField ",
                            html: "<i class='fa fa-trash'></i>",
                            on: {
                                click: function (e) {
                                    e.preventDefault();
                                    $(this).parent('div').remove();
                                    replaceNote();
                                }
                            }
                        });

                        $(this).parent("div").parent("fieldset").append(divContainerTraductionB.append(inputTraductionB).append(buttonRemoveTraduction));
                        replaceNote();
                    }
                }
            });

            createButtonTrad();

            $(".divTraduction").append(divContainerMainTraduction);
            divContainerMainTraduction.append(spanTraduction);
            divContainerMainTraduction.append(labelTraduction).append(divContainerTraduction.append(inputTraduction).append(buttonAddTraduction).append(divBtbTrad))

            replaceNote();
        }
    });

    // Add new research log
    addLog($("#textareaResearch").val(), "Etape 0 : Lancement d'une nouvelle recherche", idResearch, timeNewResearch);

    // Add Log for concept Add research
    var stringLog = createStringLogResearchConcept(arrayResearchConceptLog);
    addLog($("#textareaResearch").val(), stringLog, idResearch, new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'));

    //Add Log start research
    addLog($("#textareaResearch").val(), "Etape 2 : Commence la traduction des concepts", idResearch, new Date().addSeconds(2).toString('yyyy-MM-dd HH:mm:ss'));

    $('#divTraductionMain').show();
}

function addExclusionForm() {

    var divContainerExclusion = "";
    var inputExclusion = "";
    var buttonRemoveExclusion = "";
    var buttonAddExclusion = "";
    var divContainerExclusionB = "";
    var inputExclusionB = "";
    var idExclude;
    var inputValue;

    var data = {
        idResearch: idResearch,
        action: 'getExclusion'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: data,
        datatype: "json",
        success: function (data) {

            var objExclusion;

            if (objExclude.length > 0) {
                objExclusion = objExclude;
            } else {
                objExclusion = jQuery.parseJSON(data);
            }

            var cptExclusion = objExclusion.length;

            $(".divExclusion").empty();

            if (cptExclusion == 0) {
                cptExclusion++;
            }
            for (var i = 0; i < cptExclusion; i++) {

                if (objExclusion.length > 0) {
                    inputValue = objExclusion[i].name;
                } else {
                    inputValue = ""
                }

                divContainerExclusion = $('<div/>', {
                    class: "divContainerExclusion"
                });

                inputExclusion = $('<input/>', {
                    class: "inputExclusion ",
                    type: "text",
                    value: inputValue
                });

                buttonRemoveExclusion = $('<button />', {
                    class: "btn btn-danger buttonRemoveField ",
                    html: "<i class='fa fa-trash'></i>",
                    on: {
                        click: function (e) {
                            e.preventDefault();
                            $(this).parent('div').remove();
                            replaceNote();
                        }
                    }
                });

                buttonAddExclusion = $('<button />', {
                    class: "btn btn-success buttonAddExclusion ",
                    html: "<i class='fa fa-plus'></i>",
                    on: {
                        click: function (e) {

                            e.preventDefault();

                            divContainerExclusionB = $('<div/>', {
                                class: "divContainerExclusion"
                            });

                            inputExclusionB = $('<input/>', {
                                class: "inputExclusion ",
                                type: "text"
                            });

                            buttonRemoveExclusion = $('<button/>', {
                                class: "btn btn-danger buttonRemoveField ",
                                html: "<i class='fa fa-trash'></i>",
                                on: {
                                    click: function (e) {
                                        e.preventDefault();
                                        $(this).parent('div').remove();
                                        replaceNote();
                                    }
                                }
                            });

                            $(this).parent("div").parent("div").append(divContainerExclusionB.append(inputExclusionB).append(buttonRemoveExclusion));
                            replaceNote();
                        }
                    }
                });

                if (i == 0) {
                    $(".divExclusion").append(divContainerExclusion.append(inputExclusion).append(buttonAddExclusion));
                } else {
                    $(".divExclusion").append(divContainerExclusion.append(inputExclusion).append(buttonRemoveExclusion));
                }

            }
            $('#divExclusionMain').show();
            replaceNote();
        }
    });
}

function createButtonTrad() {

    var input;

    refGoogle = $('<div />', {
        class: "itemTrad",
        text: "Google",
        title: "Google Translate",
        on: {
            click: function (e) {
                e.preventDefault();
                input = $(this).closest(".divContainerMainTraduction").first().children('.spanTraduction').html();
                $(this).prop("alt", "https://translate.google.com/#fr/en/" + input);
                window.open($(this).prop("alt"), '_blank');
            }
        }
    });

    refLinguee = $('<div />', {
        class: "itemTrad",
        text: "Linguee",
        title: "Linguee",
        on: {
            click: function (e) {
                e.preventDefault();
                input = $(this).closest(".divContainerMainTraduction").first().children('.spanTraduction').html();
                $(this).prop("alt", "http://www.linguee.com/french-english/search?source=auto&query=%22" + input + "%22");
                showIFrame($(this), "http://www.linguee.com/french-english/search?source=auto&query=%22" + input + "%22");
            }
        }
    });

    refWordReference = $('<div />', {
        class: "itemTrad",
        text: "WordReference",
        title: "WordReference",
        on: {
            click: function (e) {
                e.preventDefault();
                input = $(this).closest(".divContainerMainTraduction").first().children('.spanTraduction').html();
                $(this).prop("alt", "http://www.wordreference.com/fren/" + input);
                showIFrame($(this), "http://www.wordreference.com/fren/" + input);
            }
        }
    });

    refGDT = $('<div />', {
        class: "itemTrad",
        alt: 'http://www.granddictionnaire.com/Resultat.aspx',
        text: "GDT",
        title: "GDT",
        on: {
            click: function (e) {
                e.preventDefault();
                showIFrame($(this), 'http://www.granddictionnaire.com/Resultat.aspx');
            }
        }
    });

    refInserm = $('<div />', {
        class: "itemTrad",
        alt: 'http://mesh.inserm.fr/mesh/search/index.jsp',
        text: "Inserm",
        title: "Mesh Inserm",
        on: {
            click: function (e) {
                e.preventDefault();
                showIFrame($(this), 'http://mesh.inserm.fr/mesh/search/index.jsp');
            }
        }
    });

    buttonTrad = $('<div/>', {
        class: "itemTrad last tooltip-all",
        html: 'Tout',
        href: "#",
        "data-title": "Pour utiliser correctement ce bouton, vous devez toujours autoriser les pop-ups du site.",
        //class : "buttonTrad btn btn-default",
        style: "",
        on: {
            click: function (e) {
                e.preventDefault();
                input = $(this).closest(".divContainerMainTraduction").first().children('.spanTraduction').html();
                window.open('http://www.granddictionnaire.com/Resultat.aspx', '_blank');
                window.open('http://mesh.inserm.fr/mesh/search/index.jsp', '_blank');
                window.open("http://www.linguee.com/french-english/search?source=auto&query=%22" + input + "%22", '_blank');
                window.open("http://www.wordreference.com/fren/" + input, '_blank');
                window.open('https://translate.google.com/#fr/en/' + input, '_blank');
                return false;
            }
        }
    });

    divBtbTrad = $('<div/>', {
        class: "btnTrads "
    });

    divBtbTrad.append(refLinguee).append(refWordReference).append(refGDT).append(refInserm).append(buttonTrad);

}

function addExclusion() {
    var arrayExclude = [];
    $('.inputExclusion').each(function () {
        var input = $.trim($(this).val());
        if (input != "") {
            arrayExclude.push(input);
        }
    });

    var dataInitial = {
        idResearch: idResearch,
        exclusion: arrayExclude,
        action: 'addExclusion'
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: dataInitial,
        datatype: "json",
        success: function (data) {
        }
    });
}

function manageButtonResearch() {

    $('#linkGoogleScholar').off("click");
    $('#linkPubMed').off("click");

    $('#linkGoogleScholar').on("click", function (e) {

        $("#divResumeSubject").removeClass("mandatory_input");

        $('#linkGoogleScholar').removeAttr("href");
        $('#linkGoogleScholar').removeAttr("target");

        var urlGoogleScholar = 'https://scholar.google.com/scholar?hl=en&as_sdt=0,5&q=';
        var allFields = "";
        var urlGoogleScholarResearch = constructQuery(urlGoogleScholar, allFields, "Google Scholar");

        if (urlGoogleScholarResearch != urlGoogleScholar) {

            $('#linkGoogleScholar').prop("href", urlGoogleScholarResearch);
            $('#linkGoogleScholar').prop("target", "_blank");
            $("#divErrorLink").text("");
            addExclusion();
        } else {
            $("#divErrorLink").text("Vous devez sélectionner des mots-clés");
            $(".buttonNextConcept").hide();
            $(".buttonNextTraduction").hide();
            $(".buttonBackTraduction").hide();
            $(".buttonBackResume").show();
            $("#divResumeSubject").addClass("mandatory_input");
        }
    });

    $('#linkPubMed').on("click", function (e) {

        $("#divResumeSubject").removeClass("mandatory_input");

        $('#linkPubMed').removeAttr("href");
        $('#linkPubMed').removeAttr("target");

        var urlPubMed = 'http://www.ncbi.nlm.nih.gov/pubmed/?term=';
        var allFields = " [All fields]";
        var urlResearchPubMed = constructQuery(urlPubMed, allFields, "PubMed");
        if (urlResearchPubMed != urlPubMed) {
            $('#linkPubMed').prop("href", urlResearchPubMed);
            $('#linkPubMed').prop("target", "_blank");
            $("#divErrorLink").text("");
            addExclusion();
        } else {
            $("#divErrorLink").text("Vous devez sélectionner des mots-clés");
            $(".buttonNextConcept").hide();
            $(".buttonNextTraduction").hide();
            $(".buttonBackTraduction").hide();
            $(".buttonBackResume").show();
            $("#divResumeSubject").addClass("mandatory_input");
        }
    });
}

function fillTradFormUpdate(objResearchUpdate) {

    var cptConcept = 1;
    var countConcept = objResearchUpdate[0].length;
    $(".divTraduction").empty();
    var divContainerMainTraduction = "";
    var spanTraduction = "";
    var labelTraduction = "";
    var inputTraduction = "";
    var buttonRemoveTraduction = "";
    var buttonAddTraduction = "";
    var divContainerTraductionB = "";
    var inputTraductionB = "";
    var divContainerTraduction = "";
    var arrayResearchConceptLog = new Array();

    arrayResearchConceptLog.push($("#textareaResearch").val());

    for (var i = 0; i < countConcept; i++) {

        var inputValue = objResearchUpdate[0][i][1];
        var cptConcept = i + 1;
        var countTraduction = objResearchUpdate[0][i][2].length;

        //Add concept to array for log
        arrayResearchConceptLog.push(inputValue);

        divContainerMainTraduction = $('<fieldset/>', {
            class: "divContainerMainTraduction"
        });

        spanTraduction = $('<span/>', {
            class: "spanTraduction",
            html: inputValue
        });

        labelTraduction = $('<legend/>', {
            class: "labelTraduction",
            html: "Concept " + cptConcept + " : "
        });

        divContainerMainTraduction.append(labelTraduction);
        divContainerMainTraduction.append(spanTraduction);

        nameClass = 'divContainerTraduction';

        for (var j = 0; j < countTraduction; j++) {

            if (j > 0) {
                nameClass = 'divContainerTraduction sup';
            }

            divContainerTraduction = $('<div/>', {
                class: nameClass
            });

            inputTraduction = $('<input/>', {
                class: "inputTraduction ",
                type: "text",
                value: objResearchUpdate[0][i][2][j]
            });

            buttonRemoveTraduction = $('<button />', {
                class: "btn btn-danger buttonRemoveField ",
                html: "<i class='fa fa-trash'></i>",
                on: {
                    click: function (e) {
                        e.preventDefault();
                        $(this).parent('div').remove();
                        replaceNote();
                    }
                }
            });

            buttonAddTraduction = $('<button />', {
                class: "btn btn-success buttonAddTraduction ",
                html: "<i class='fa fa-plus'></i>",
                on: {
                    click: function (e) {

                        e.preventDefault();

                        divContainerTraductionB = $('<div/>', {
                            class: "divContainerTraduction sup"
                        });

                        inputTraductionB = $('<input/>', {
                            class: "inputTraduction ",
                            type: "text"
                        });

                        buttonRemoveTraduction = $('<button />', {
                            class: "btn btn-danger buttonRemoveField ",
                            html: "<i class='fa fa-trash'></i>",
                            on: {
                                click: function (e) {
                                    e.preventDefault();
                                    $(this).parent('div').remove();
                                    replaceNote();
                                }
                            }
                        });
                        $(this).parent("div").parent("fieldset").append(divContainerTraductionB.append(inputTraductionB).append(buttonRemoveTraduction));
                        replaceNote();
                    }
                }
            });


            if (j == 0) {
                createButtonTrad();
                divContainerTraduction.append(inputTraduction);
                divContainerTraduction.append(buttonAddTraduction);
                divContainerMainTraduction.append(divBtbTrad);
            } else {
                divContainerTraduction.append(inputTraduction);
                divContainerTraduction.append(buttonRemoveTraduction);
            }

            divContainerMainTraduction.append(divContainerTraduction);
        }

        $(".divTraduction").append(divContainerMainTraduction);

        replaceNote();
    }

    // Add Log for concept update research
    var stringLog = createStringLogResearchConcept(arrayResearchConceptLog);
    addLog($("#textareaResearch").val(), stringLog, idResearch, new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'));

    //Add Log start research
    addLog($("#textareaResearch").val(), "Etape 2 : Commence la traduction des concepts", idResearch, new Date().addSeconds(2).toString('yyyy-MM-dd HH:mm:ss'));
}


function fillConceptFormUpdate(objResearchUpdate) {
    backToConceptStep();
    $("#divResumeMain").hide();

    $(".inputConcept").val("");

    if (objResearchUpdate.length != 0) {

        $("#textareaResearch").val(objResearchUpdate.name);

        var cptConcept = objResearchUpdate[0].length;

        for (var i = 0; i < cptConcept; i++) {

            var conceptName = objResearchUpdate[0][i][1];

            if (conceptName != null) {
                $(".inputConcept:eq(" + i + ")").val(conceptName);
            }
        }
    }
}

function backToConceptStep() {
    $("#labelSubject").show();
    $("#divConceptMain").show();
    $("textarea").prop("disabled", false);
    $("#divTraductionMain").hide();
    $("#panel").addClass('fullPanel');
    $("#panel").removeClass('leftPanel');
    $("#iFrame").hide();
    $(".buttonNextConcept").show();
    $(".buttonNextTraduction").hide();
    $(".buttonBackTraduction").hide();
    $(".buttonBackResume").hide();
}

function startLoad() {
}

function endLoad() {
    $('#panel').show();
    $('#div_note').css("visibility", "visible");
    $(".sticky").show();
    $('#overlay').hide();
}

function replaceNote() {

    // end loading
    endLoad();

    // Change place note
    var position_div_note = $("#div_note").offset();
    var bottom_div_note = position_div_note.top + $("#div_note").outerHeight();
    $(".sticky").each(function () {
        if ($(this).find(".sticky-content").hasClass("reduce")) {
            var id_sticky = $(this).prop("id");
            $("#" + id_sticky).css("top", +bottom_div_note - 100);
        }
    });
}

function backToTraductionStep() {
    $("#divResumeMain").hide();
    $("#divTraductionMain").show();
    $(".buttonNextConcept").hide();
    $(".buttonBackTraduction").show();
    $(".buttonNextTraduction").show();
    $(".buttonBackResume").hide();
    $("#divErrorLink").text("");

    var cpt = 0;
    objExclude = [];
    $('.inputExclusion').each(function () {
        var input = $.trim($(this).val());
        if (input != "") {
            item = {};
            item["name"] = input;
            objExclude[cpt] = item;
        }
        cpt++;
    });
}

function goToTraductionStep() {

    $(".inputConcept").removeClass("mandatory_input");
    $("#textareaResearch").removeClass("mandatory_input");

    var mandatory = true;
    var textMandatory = "";
    var empty = [];

    if ($("#textareaResearch").val().length > 0) {
        $('.inputConcept').each(function () {
            if ($.trim($(this).val()).length > 0) {
                mandatory = false;
                textMandatory = "Vous devez remplir au minimum un concept"
                empty.push(false);
            } else {
                empty.push(true);
            }
        });
    } else {
        $("#textareaResearch").addClass("mandatory_input");
        textMandatory = "Vous devez remplir au minimum un sujet de recherche"
    }

    for (var i = 0; i < empty.length; i++) {
        if (empty[i] == true) {
            if (i == 0) {
                mandatory = true;
                textMandatory = "Vous devez remplir le concept " + (i + 1);
                $(".inputConcept:eq(" + i + ")").addClass("mandatory_input");
                break;
            } else if (i == 1) {
                if (empty[i + 1] == false || empty[i + 2] == false) {
                    mandatory = true;
                    textMandatory = "Vous devez remplir le concept " + (i + 1);
                    $(".inputConcept:eq(" + (i) + ")").addClass("mandatory_input");
                    break;
                }
            } else if (i == 2) {
                if (empty[i + 1] == false) {
                    mandatory = true;
                    textMandatory = "Vous devez remplir le concept " + (i + 1);
                    $(".inputConcept:eq(" + (i) + ")").addClass("mandatory_input");
                    break;
                }
            }
        }
    }

    if (!mandatory) {
        $('#divTraductionMain').show();
        $("#labelSubject").hide();
        $("#divConceptMain").hide();
        $(".buttonNextConcept").hide();
        $("textarea").prop("disabled", true);
        $("#divErrorConcept").text("");
        $("#divErrorTraduction").text("");
        $(".buttonNextConcept").hide();
        $(".buttonBackTraduction").show();
        $(".buttonNextTraduction").show();
        $(".buttonBackResume").hide();
    }
    else {
        $("#divErrorConcept").text(textMandatory);
        $(".buttonNextConcept").show();
        $(".buttonNextTraduction").hide();
        $(".buttonBackTraduction").hide();
        $(".buttonBackResume").hide();
    }

    return mandatory;
}

function addLog(actionLog, textLog, idResearch, time) {

    if (admin == false) {
        var dataInitial = {
            actionLog: actionLog,
            textLog: textLog,
            idUser: idUser,
            time: time,
            idResearch: idResearch,
            action: 'addLog'
        }

        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: dataInitial,
            datatype: "json",
            success: function (data) {
            }
        });
    }
}

function constructQuery(search_url, allFields, browser) {

    var url = search_url;
    var value = '';
    var rowToCompare = 0;
    var fields = allFields;
    var countDiv = $('.divContainerResumeConcept').length;

    for (var i = 0; i < countDiv; i++) {
        $('> .divContainerResumeKeywords', $('.divContainerResumeConcept')[i]).each(function () {
            $('.backgroundSelected', this).each(function () {

                var keyword = $(this).text();

                if (i == rowToCompare) {
                    if (value.length == 0) {
                        value = "(" + '"' + $.trim(keyword) + '"' + fields;
                    } else {
                        value += " OR " + '"' + $.trim(keyword) + '"' + fields;
                    }
                } else {
                    rowToCompare = i;

                    if (value.length == 0) {
                        value = "(" + '"' + $.trim(keyword) + '"' + fields;
                    } else {
                        value += ")" + " AND " + "(" + '"' + $.trim(keyword) + '"' + fields;
                    }
                }
            });
        });
    }

    value = value + ")";

    $('.inputExclusion').each(function () {
        var word = $.trim($(this).val());
        if (word.length > 0) {
            if (search_url == 'https://scholar.google.com/scholar?hl=en&as_sdt=0,5&q=') {
                value += ' -' + '"' + word + '"';
            } else {
                value += ' NOT ' + ' ("' + word + '" [All fields])';
            }
        }
    });

    var stringLog = "Etape 4 : A effectué une recherche avec " + browser + " la requête suivante : " + value;

    if (value.substr(0, 1) != ")") {
        addLog($("#divSubject textarea").val(), stringLog, idResearch, new Date().addSeconds(1).toString('yyyy-MM-dd HH:mm:ss'));
        url += value;
    }

    return url;
}

function hasWhiteSpace(s) {
    return /\s/g.test(s);
}

function showIFrame(current, url) {

    $("#panel").removeClass('fullPanel');
    $("#panel").addClass('leftPanel');
    $("#iFrame").css("display", "block");
    $("#iFrame").prop("src", url);
    $("#buttonCloseIframe").show();
}

function sendResearchToServerInitial() {
    var research = [$.trim($("#textareaResearch").val())];
    var conceptArray = [];

    for (var i = 0; i < 4; i++) {
        var conceptName = $.trim($(".inputConcept:eq(" + i + ")").val());
        conceptArray.push(conceptName);
    }

    research.push(conceptArray);

    var formData = {
        idUser: idUser,
        action: 'addResearchInitial',
        idResearch: idResearch,
        research: research,
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: formData,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);
            idResearch = obj.reponse;

            if (update == false) {
                var objResearchNew = new Array();
                objResearchNew = new Array();
                var arrayTraduction;
                var cptConcept = 0;

                objResearchNew.push(new Array());

                objResearchNew.name = $("#textareaResearch").val();

                if ($('.divContainerMainTraduction')[cptConcept]) {
                    $('.divContainerMainTraduction').each(function () {
                        var concept = $(".spanTraduction:eq(" + cptConcept + ")").text();
                        if (concept != "") {
                            objResearchNew[0].push(new Array("id", concept));

                            arrayTraduction = new Array();

                            $('> .divContainerTraduction', this).each(function () {
                                $('> .inputTraduction', this).each(function () {
                                    var traduction = $(this).val();

                                    if (traduction != "") {
                                        arrayTraduction.push(traduction);
                                    } else {
                                        arrayTraduction.push("");
                                    }
                                });
                            });
                            objResearchNew[0][cptConcept].push(arrayTraduction);
                        }
                        cptConcept++;
                    });
                    var objResearch = compareResearch(objResearchNew)
                    fillTradFormUpdate(objResearch);
                } else {
                    fillTradFormNew();
                }

                $('.sticky').each(function () {
                    var that = $(this), sticky = (that.hasClass("sticky-status") || that.hasClass("sticky-content")) ? that.parents('div.sticky') : that,
                        note = {
                            top: sticky.css("top"),
                            left: sticky.css("left"),
                            text: sticky.children(".sticky-content").html(),
                            id: sticky.attr("id"),
                            title: sticky.find(".sticky-status").text()
                        }
                    $.ajax({
                        url: "ajax.php",
                        type: "POST",
                        data: {note: note, idResearch: idResearch, action: "addNote"},
                        datatype: "json",
                        success: function (data) {
                        }, error: function () {
                        }
                    });
                });

            } else {
                idResearch = $("#listResearch").children(":selected").prop("value");

                var data = {
                    idResearch: idResearch,
                    action: 'getResearch'
                }

                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: data,
                    datatype: "json",
                    success: function (data) {
                        objResearchUpdate = jQuery.parseJSON(data);
                        var objResearch = compareResearch(objResearchUpdate)
                        fillTradFormUpdate(objResearch);
                    }
                });
            }
        }
    });
}

function compareResearch(objResearchUpdate) {
    var objResearchNew = new Array();

    objResearchNew.push(new Array());

    for (var i = 0; i < 4; i++) {
        var conceptName = $(".inputConcept:eq(" + i + ")").val();

        if (conceptName != "") {
            if (i >= objResearchUpdate[0].length) {
                objResearchNew[0].push(new Array("id", $(".inputConcept:eq(" + i + ")").val()));
                objResearchNew[0][objResearchNew[0].length - 1].push(new Array(""));
            } else {
                objResearchNew[0].push(new Array(objResearchUpdate[0][i][0], $(".inputConcept:eq(" + i + ")").val()));

                if ($(".inputConcept:eq(" + i + ")").val() == objResearchUpdate[0][i][1]) {
                    if (objResearchUpdate[0][i][2].length > 0) {
                        objResearchNew[0][objResearchNew[0].length - 1].push(objResearchUpdate[0][i][2]);
                    } else {
                        objResearchNew[0][objResearchNew[0].length - 1].push(new Array(""));
                    }
                } else {
                    objResearchNew[0][objResearchNew[0].length - 1].push(new Array(""));
                }
            }
        }
    }

    objResearchNew.name = $("#textareaResearch").val();

    return objResearchNew;
}
function sendResearchToServerFinal() {

    var research = [$.trim($("#textareaResearch").val())];
    var rowCount = $('.divContainerMainTraduction').length;
    var conceptArray = [];
    var stringLogTableKeyWords = "Etape 2 : Terminé en validant la recherche " + research + " ";


    for (var i = 0; i < rowCount; i++) {

        var currentDivContainerMainTraduction = $('.divContainerMainTraduction')[i];
        var columnCount = $(currentDivContainerMainTraduction).children('.divContainerTraduction').length;

        var conceptName = $.trim($(currentDivContainerMainTraduction).find(".spanTraduction").text());
        conceptArray.push(conceptName);

        if (i == 0) {
            stringLogTableKeyWords = stringLogTableKeyWords + " ayant comme concept " + conceptName + " ";
        } else {
            stringLogTableKeyWords = stringLogTableKeyWords + " et " + conceptName + " ";
        }

        var traductionArray = [];
        for (var j = 0; j < columnCount; j++) {
            var currentDivContainerTraduction = $(currentDivContainerMainTraduction).children('.divContainerTraduction').eq(j);
            var traductionName = $.trim($(currentDivContainerTraduction).children(".inputTraduction").val());

            if (traductionName != null) {
                traductionArray.push(traductionName);

                if (columnCount == 1) {
                    stringLogTableKeyWords = stringLogTableKeyWords + " (" + traductionName + ")";
                } else if (j == 0) {
                    stringLogTableKeyWords = stringLogTableKeyWords + " (" + traductionName + " ";
                } else if (j == columnCount - 1) {
                    stringLogTableKeyWords = stringLogTableKeyWords + " / " + traductionName + ")";
                } else {
                    stringLogTableKeyWords = stringLogTableKeyWords + " / " + traductionName + " ";
                }
            }
        }
        conceptArray.push(traductionArray);
    }

    research.push(conceptArray);

    addLog($("#textareaResearch").val(), stringLogTableKeyWords, idResearch, new Date().toString('yyyy-MM-dd HH:mm:ss'));

    addLog($("#textareaResearch").val(), "Etape 3 : Lance l'étape de vérification  d'usage des termes retenus et l’existence d’expressions équivalentes", idResearch, new Date().addSeconds(2).toString('yyyy-MM-dd HH:mm:ss'));

    $(".divClick").each(function (index) {
        var line = [];
        line.push($(this).text());
    });

    var formData = {
        idUser: idUser,
        action: 'addResearchFinal',
        research: research,
        idResearch: idResearch
    }

    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: formData,
        datatype: "json",
        success: function (data) {
            var obj = jQuery.parseJSON(data);

            idResearch = obj.reponse;

            addExclusionForm();

            $('.sticky').each(function () {
                var that = $(this), sticky = (that.hasClass("sticky-status") || that.hasClass("sticky-content")) ? that.parents('div.sticky') : that,
                    note = {
                        top: sticky.css("top"),
                        left: sticky.css("left"),
                        text: sticky.children(".sticky-content").html(),
                        id: sticky.attr("id"),
                        title: sticky.find(".sticky-status").text()
                    }
                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {note: note, idResearch: idResearch, action: "addNote"},
                    datatype: "json",
                    success: function (data) {
                    }, error: function () {
                    }
                });
            });

        }
    });
}

function createStringLogResearchConcept(arrayResearchConceptLog) {

    var conceptString = "";

    for (var i = 1; i < arrayResearchConceptLog.length; i++) {
        if (arrayResearchConceptLog[i] != null) {
            if (arrayResearchConceptLog[i] != arrayResearchConceptLog[arrayResearchConceptLog.length - 1]) {
                conceptString = conceptString + arrayResearchConceptLog[i] + " / ";
            } else {
                conceptString = conceptString + arrayResearchConceptLog[i];
            }
        }
    }

    return "Etape 1 : Terminé en validant les concepts " + conceptString + " pour la  recherche " + arrayResearchConceptLog[0];
}

function fillTableKeyWords() {

    $(".inputTraduction").removeClass("mandatory_input");
    $("#divResumeSubject").removeClass("mandatory_input");

    var mandatory = false;
    $(".buttonNextConcept").hide();
    $(".buttonBackTraduction").hide();
    $(".buttonNextTraduction").hide();
    $(".buttonBackResume").show();
    $("#buttonCloseIframe").hide();

    $('.inputTraduction').each(function () {
        if ($(this).val().length == 0) {
            mandatory = true;
            replaceNote();
            $(this).addClass("mandatory_input");
            return;
        }
    });

    if (mandatory == false) {
        $("#panel").addClass('fullPanel');
        $("#panel").removeClass('leftPanel');
        $("#iFrame").hide();

        $('#divResumeMain').show();
        $("#divTraductionMain").hide();

        var divResumeConcept = "";
        var divContainerResumeConcept = "";
        var divContainerResumeKeywords = "";
        var divClick = "";

        $('#divResumeSubject').empty();

        var countFieldset = 0;
        $('.divContainerMainTraduction').each(function () {

            divContainerResumeConcept = $('<div/>', {
                class: "divContainerResumeConcept"
            });

            divResumeConcept = $('<div/>', {
                class: "tdConcept",
                html: '<div class="divHead" style="display: none;">' + $(this).find('.spanTraduction').text() + '</div>'
            });

            divContainerResumeKeywords = $('<fieldset/>', {
                class: "divContainerResumeKeywords",
                html: "<legend class='labelTraduction'>" + $(this).find('.spanTraduction').text() + "</legend>"
            });


            $('> .divContainerTraduction', this).each(function () {

                divClick = $('<div/>', {
                    html: $('> .inputTraduction', this).val(),
                    class: "divClick"
                });

                divContainerResumeKeywords.append(divClick);
            });
            if (countFieldset > 0) {
                divContainerResumeConcept.append('<div id="DivAnd"><i class="fa fa-plus"></i></div>');
            }
            divContainerResumeConcept.append(divResumeConcept);
            divContainerResumeConcept.append(divContainerResumeKeywords)
            $('#divResumeSubject').append(divContainerResumeConcept);
            countFieldset++;
        });

        manageButtonResearch();

        $('.divClick').click(function (e) {

            if ($(this).hasClass("backgroundSelected")) {
                $(this).addClass('backgroundNotSelected');
                $(this).removeClass('backgroundSelected');
            } else {
                $(this).removeClass('backgroundNotSelected');
                $(this).addClass('backgroundSelected');
            }

            $('#linkGoogleScholar').removeClass("notactive");
            $('#linkGoogleScholar').addClass("active");
            $('#linkPubMed').removeClass("notactive");
            $('#linkPubMed').addClass("active");

            if ($('#linkPubMed').prop("href")) {
                $('#linkPubMed').removeAttr("href");
                $('#linkPubMed').removeAttr("target");
            }

            if ($('#linkGoogleScholar').prop("href")) {
                $('#linkGoogleScholar').removeAttr("href");
                $('#linkGoogleScholar').removeAttr("target");
            }
        });

        if (admin == false) {
            sendResearchToServerFinal();
        } else {
            addExclusionForm();
            replaceNote();
        }
        $("#divErrorTraduction").text("");
    } else {
        $("#divErrorTraduction").text("Vous devez remplir tous les champs");
        $(".buttonNextConcept").hide();
        $(".buttonNextTraduction").show();
        $(".buttonBackTraduction").show();
        $(".buttonBackResume").hide();
        replaceNote();
    }
}

function hideNote() {
    if ($("#tabCurrent").css("display") == "block") {
        STICKIES.open();
        $(".sticky").css("display", "block");
        $("#div_note").css("display", "block");
        $("#divSticky").css("display", "block");
    } else {
        $(".sticky").css("display", "none");
        $("#div_note").css("display", "none");
        $("#divSticky").css("display", "none");
    }
}

function hideFooter() {
    if ($("#tabCurrent").css("display") == "block") {
        $("footer").css("display", "none");
    } else {
        $("footer").css("display", "block");
    }
}
<?php include 'header.php'; ?>
<script type="text/javascript" src="js/forget.js"></script>
<div id="titleLogin">
	<h1>malin</h1>
</div>
<div id="subTitleLogin">
	E-Création 2.0
</div>
<div id="divForget">

	<form id="formForget" class="addForm" action='' method='post' >
		<h4>Mot de passe oublié</h4>

			<label id="labelPseudoForget">Pseudo</span></label>
			<input id="inputPseudoForget" type='text' />

		<div class="form_center_div">
			<input id="buttonForget" class="submitButton btn btn-primary" type='submit' value='Envoyer' name="save" form="formForget"/>
		</div>

		<label id="errorForget" class="error"></label>
		<label id="messageSuccessForget" class="success"></label>
		<label id="messageErrorForget" class="error"></label>

	</form>
</div>
</body>
<?php
include 'footer.php';
?>
</html>

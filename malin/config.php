<?php

// Connection au serveur
try {

	// COMPLETER LES INFORMATIONS CI-DESSOUS
	$dns = 'mysql:host=XXX;dbname=XXX';
	$dbuser = "XXX";
	$dbpass = "XXX";

	// Options de connection
	$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

	// Initialisation de la connection
	$connection = new PDO($dns, $dbuser, $dbpass, $options);
	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	print "Erreur : " . $e -> getMessage();
	die();
}
?>

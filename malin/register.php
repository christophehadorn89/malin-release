<?php include 'header.php'; ?>

<script type="text/javascript" src="js/register.js"></script>

<div id="titleLogin">
	<h1>malin</h1>
</div>
<div id="subTitleLogin">
	E-Création 2.0
</div>

		<div id="divRegister">
			<div>
				
			</div>
			<form id="formRegister" class="addForm" action='' method='post' >
				<h4>Créer un compte</h4>

					<label id="labelNameRegister">Pseudo</label>
					<input id="inputNameRegister" type='text' />

					<label id="labelEmailRegister">Email</label>
					<input id="inputEmailRegister" type='text' />

					<label id="labelPasswordRegister">Mot de passe</label>
					<input id="inputPasswordRegister" type='password' />

					<label id="labelStatutRegister">Statut</label>
					<select id="selectStatutRegister">
							<option value="etudiant">Etudiant</option>
							<option value="professionnel">Professionnel</option>
					</select>

				<div class="form_center_div">
					<input id="buttonAddRegister" class="submitButton btn btn-primary" type='submit' value='Enregistrer' name="save" form="formRegister"/>
				</div>

				<label id="errorRegister" class="error"></label>
				<label id="messageSuccessRegister" class="success"></label>
				<label id="messageErrorRegister" class="error"></label>
			</form>
		</div>
<div id="divInscription">
</div>
	</body>
<?php
include 'footer.php';
?>
</html>

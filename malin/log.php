<?php
/**
 * Created by PhpStorm.
 * User: christop.hadorn
 * Date: 28.10.2015
 * Time: 12:56
 */
// Ouverture d'une connexion à la base de données
require_once ('config.php');

$connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $query = $connection -> prepare("SELECT hour, pseudo, statut, action, text, fk_research FROM log, user WHERE log.fk_user = user.id ORDER BY fk_research,hour ASC");

    // output headers so that the file is downloaded rather than displayed

    header('Content-Encoding: UTF-8');
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=log.txt');
    echo "\xEF\xBB\xBF"; // UTF-8 BOM

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');

    // Execution des requêtes
    try {
        $query->execute();
        $record = $query -> fetchAll();
        $numberRow = 0;
        $timeTotal = 0;
        $seconds = 0;
        $rowHeader = "N°;Pseudo;Statut;Recherche;Texte;Date;Temps passé";

        // output the column headings
        fputcsv($output, explode(';',$rowHeader), ';');

        foreach ($record as $row) {
            $numberRow++;
            $hour = trim($row['hour']);
            $text = trim($row['text']);
            $pseudo = trim($row['pseudo']);
            $statut = trim($row['statut']);
            $action = trim($row['action']);
            $timeCurrent = date_create($hour);
            $time = $timeCurrent->format('d/m/Y à H:i:s');

            if(isset($fk_research)){
                if($fk_research != $row['fk_research']){
                    $timeTotal = 0;
                    $seconds = 0;
                    unset($timePrevious);
                    unset($fk_research);
                }
            } else {
                $fk_research = $row['fk_research'];
            }

            // Date diff between 2 dates
            if(isset($timePrevious)){
                $timeCurrent->format('d/m/Y  H:i:s');
                $timePrevious->format('d/m/Y  H:i:s');
                $seconds = setSeconds(date_diff($timePrevious, $timeCurrent));
                // Sum total time
                if(strpos($text,'Etape 0 :') === false){
                    $timeTotal = (int)($timeTotal + $seconds);
                }
            }
            //Set previous hour
            $timePrevious = $timeCurrent;

            // output the column headings
            $row = $numberRow . ';' . $pseudo . ';' . $statut . ';' . $action . ';' . $text . ';' . $time . ';' . gmdate("H:i:s", $timeTotal);
            fputcsv($output, explode(';',$row), ';');
        }
    } catch (Exception $e) {
    }
fclose($output);

function setSeconds($delta){
    return ($delta->format("%s"))
    + ($delta->format("%i") * 60)
    + ($delta->format("%h") * 60 * 60)
    + ($delta->format("%d") * 60 * 60 * 24)
    + ($delta->format("%m") * 60 * 60 * 24 * 30)
    + ($delta->format("%y") * 60 * 60 * 24 * 365);
}


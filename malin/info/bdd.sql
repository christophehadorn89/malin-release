-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2016 at 04:51 PM
-- Server version: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cyberlearnmobiledevch29`
--

-- --------------------------------------------------------

--
-- Table structure for table `concept`
--

CREATE TABLE IF NOT EXISTS `concept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_research` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_research` (`fk_research`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1193 ;

-- --------------------------------------------------------

--
-- Table structure for table `exclusion`
--

CREATE TABLE IF NOT EXISTS `exclusion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_research` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_research` (`fk_research`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hour` datetime NOT NULL,
  `action` text CHARACTER SET utf8 NOT NULL,
  `text` text CHARACTER SET utf8 NOT NULL,
  `fk_user` int(11) NOT NULL,
  `fk_research` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkuserlog` (`fk_user`),
  KEY `fk_research` (`fk_research`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4439 ;

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `toppx` varchar(10) NOT NULL,
  `leftpx` varchar(10) NOT NULL,
  `text` text NOT NULL,
  `title` varchar(50) NOT NULL,
  `fk_research` int(11) NOT NULL,
  `uniqueid` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_research` (`fk_research`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `research`
--

CREATE TABLE IF NOT EXISTS `research` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=393 ;

-- --------------------------------------------------------

--
-- Table structure for table `traduction`
--

CREATE TABLE IF NOT EXISTS `traduction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `fk_concept` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_concept` (`fk_concept`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `statut` varchar(50) NOT NULL,
  `annee` varchar(4) NOT NULL,
  `authorize_log` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `pseudo`, `password`, `email`, `statut`, `annee`, `authorize_log`) VALUES
(1, 'admin', '9038d21dfe53e680f087fdc0bebbdd44', '', 'Professionnel', '2015', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_research`
--

CREATE TABLE IF NOT EXISTS `user_research` (
  `fk_user` int(11) NOT NULL,
  `fk_research` int(11) NOT NULL,
  KEY `index` (`fk_user`),
  KEY `index1` (`fk_research`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `concept`
--
ALTER TABLE `concept`
  ADD CONSTRAINT `concept_ibfk_1` FOREIGN KEY (`fk_research`) REFERENCES `research` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exclusion`
--
ALTER TABLE `exclusion`
  ADD CONSTRAINT `exclusion_ibfk_1` FOREIGN KEY (`fk_research`) REFERENCES `research` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `log_ibfk_2` FOREIGN KEY (`fk_research`) REFERENCES `research` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_ibfk_1` FOREIGN KEY (`fk_research`) REFERENCES `research` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `traduction`
--
ALTER TABLE `traduction`
  ADD CONSTRAINT `traduction_ibfk_1` FOREIGN KEY (`fk_concept`) REFERENCES `concept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_research`
--
ALTER TABLE `user_research`
  ADD CONSTRAINT `user_research_ibfk_1` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_research_ibfk_2` FOREIGN KEY (`fk_research`) REFERENCES `research` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

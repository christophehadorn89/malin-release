### Projet MALIN - E-Création 2.0 ###
------------------------------------------------------------------------------------------------------------------------

** Résumé **
La recherche documentaire est un élément essentiel de la mise en oeuvre de pratiques basées sur des résultats de recherche.
MALIN (Moyens d’Aide à la Littératie Informationnelle et Numérique) est un projet visant à développer des outils et des cours
en ligne pour soutenir l'acquisition de compétence en recherche documentaire. Cet outil de recherche de mots-clés en est le premier élément.
Il a été développé conjointement par la filière ergothérapie de la Haute Ecole de Travail Social et de la Santé - EESP,
par la filière physiothérapie de la Haute Ecole de Santé de Genève et par le Centre e-learning // Cyberlearn de la Haute École Spécialisée de Suisse Occidentale.

** Version **
Version 1.0

** Code source **
https://bitbucket.org/christophehadorn89/malin/

------------------------------------------------------------------------------------------------------------------------

### PRE-REQUIS ###

APACHE  2.4.7
MYSQL  5.6.15
PHP  5.5.8

------------------------------------------------------------------------------------------------------------------------

### INSTALLATION ###

1) Déposer le répetoire malin sur votre serveur web

2) Créer une nouvelle base de données MYSQL

3) Importer les tables dans votre base de données à l'aide du script (bdd.sql)

4) Compléter vos informations de connection vers votre base de données (config.php)
$dns = 'mysql:host=XXX;dbname=XXX';
$dbuser = "XXX";
$dbpass = "XXX";

------------------------------------------------------------------------------------------------------------------------

### COMPTE ADMIN ###

Nom d'utilisateur : admin
Mot de passe : ecreationmalin
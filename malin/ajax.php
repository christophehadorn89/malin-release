<?php
// Ouverture d'une connexion à la base de données
require_once('config.php');

/*
 * Variable to recieve
 */
$GLOBALS['salt'] = 'aAéà@#';
$action = (!empty($_POST['action']) ? $_POST['action'] : '');
$pseudo = (!empty($_POST['pseudo']) ? $_POST['pseudo'] : '');
$password = (!empty($_POST['password']) ? $_POST['password'] : '');
$statut = (!empty($_POST['statut']) ? $_POST['statut'] : '');
$idUser = (!empty($_POST['idUser']) ? $_POST['idUser'] : '');
$idResearch = (!empty($_POST['idResearch']) ? $_POST['idResearch'] : '');
$research = (!empty($_POST['research']) ? $_POST['research'] : '');
$actionLog = (!empty($_POST['actionLog']) ? $_POST['actionLog'] : '');
$textLog = (!empty($_POST['textLog']) ? $_POST['textLog'] : '');
$note = (!empty($_POST['note']) ? $_POST['note'] : '');
$log = (!empty($_POST['log']) ? $_POST['log'] : '');
$time = (!empty($_POST['time']) ? $_POST['time'] : '');
$email = (!empty($_POST['email']) ? $_POST['email'] : '');
$exclusion = (!empty($_POST['exclusion']) ? $_POST['exclusion'] : '');

switch ($action) {
    case 'addExclusion':
        echo addExclusion($exclusion, $idResearch);
        break;
    case 'getExclusion':
        echo getExclusion($idResearch);
        break;
    case 'getNote':
        echo getNote($idResearch);
        break;
    case 'deleteNote':
        echo deleteNote($note);
        break;
    case 'addNote':
        echo addNote($note, $idResearch);
        break;
    case 'addLog':
        echo addLog($actionLog, $textLog, $idUser, $idResearch, $time);
        break;
    case 'addLogProfil':
        echo addLogProfil($actionLog, $textLog, $idUser, $time);
        break;
    case 'getLog':
        echo getLog($idUser);
        break;
    case 'deleteResearch':
        echo deleteResearch($idResearch);
        break;
    case 'getUserList':
        echo getUserList($time, $statut);
        break;
    case 'getYearList':
        echo getYearList();
        break;
    case 'getStatutList':
        echo getStatutList($time);
        break;
    case 'login' :
        echo getUser($pseudo, $password);
        break;
    case 'register' :
        echo addUser($pseudo, $password, $statut, $email);
        break;
    case 'forget' :
        echo sendPassword($pseudo);
        break;
    case 'addResearchInitial' :
        echo addResearchInitial($research, $idUser, $idResearch);
        break;
    case 'addResearchFinal' :
        echo addResearchFinal($research, $idResearch);
        break;
    case 'getUser' :
        echo getUserProfil($idUser);
        break;
    case 'updateUser' :
        echo updateUserProfil($idUser, $pseudo, $password, $log, $statut, $email);
        break;
    case 'getResearchList' :
        echo getResearchList($idUser);
        break;
    case 'getResearch' :
        echo getResearch($idResearch);
        break;
}

function getExclusion($idResearch)
{
    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $query = $connection->prepare("SELECT * FROM exclusion WHERE fk_research = :id");
    $query->bindParam(':id', $idResearch);

    try {
        $query->execute();
        $record = $query->fetchAll();

        $array = array();

        foreach ($record as $row) {

            $id = $row['id'];
            $name = $row['name'];

            $arrayRow = array();

            $arrayRow["id"] = $id;
            $arrayRow["name"] = $name;

            array_push($array, $arrayRow);
        }
    } catch (Exception $e) {
    }

    return json_encode($array);
}

function getNote($idResearch)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $query = $connection->prepare("SELECT * FROM note WHERE fk_research = :id");
    $query->bindParam(':id', $idResearch);

    try {
        $query->execute();
        $record = $query->fetchAll();

        $arrayNote = array();

        foreach ($record as $row) {
            $top = $row['toppx'];
            $left = $row['leftpx'];
            $text = $row['text'];
            $uniqueid = $row['uniqueid'];
            $title = $row['title'];

            $arrayNoteRow = array();

            $arrayNoteRow["top"] = $top;
            $arrayNoteRow["left"] = $left;
            $arrayNoteRow["text"] = $text;
            $arrayNoteRow["id"] = $uniqueid;
            $arrayNoteRow["title"] = $title;

            array_push($arrayNote, $arrayNoteRow);
        }
    } catch (Exception $e) {
    }

    return json_encode($arrayNote);
}

function deleteNote($note)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $query = $connection->prepare("DELETE FROM note WHERE uniqueid = :uniqueid");
    $query->bindParam(':uniqueid', $note["id"]);

    // Execution des requêtes
    try {
        $query->execute();
        $reponse = true;
    } catch (Exception $e) {
        echo $e->getMessage();
        $reponse = false;
    }
    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addNote($note, $idResearch)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryInsert = $connection->prepare("insert into note (toppx, leftpx, text, title, fk_research, uniqueid) values (:toppx, :leftpx, :text, :title, :fk_research, :uniqueid)");
    $queryInsert->bindParam(':toppx', $note["top"]);
    $queryInsert->bindParam(':leftpx', $note["left"]);
    $queryInsert->bindParam(':text', $note["text"]);
    $queryInsert->bindParam(':fk_research', $idResearch);
    $queryInsert->bindParam(':uniqueid', $note["id"]);
    $queryInsert->bindParam(':title', $note["title"]);

    $querySelect = $connection->prepare("SELECT COUNT(*) as count FROM note WHERE uniqueid = :uniqueid");
    $querySelect->bindParam(':uniqueid', $note["id"]);

    $queryUpdate = $connection->prepare("UPDATE note SET toppx =:toppx, leftpx=:leftpx, text=:text, title=:title WHERE uniqueid = :uniqueid");
    $queryUpdate->bindParam(':uniqueid', $note["id"]);
    $queryUpdate->bindParam(':toppx', $note["top"]);
    $queryUpdate->bindParam(':leftpx', $note["left"]);
    $queryUpdate->bindParam(':text', $note["text"]);
    $queryUpdate->bindParam(':title', $note["title"]);

    // Execution des requêtes
    try {
        // Count if a note already exist
        $querySelect->execute();
        $count = $querySelect->fetchColumn();

        if ($count > 0) {
            $queryUpdate->execute();
        } else {
            $queryInsert->execute();
        }
        $reponse = true;
    } catch (Exception $e) {
        echo $e->getMessage();
        $reponse = false;
    }
    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addLog($actionLog, $textLog, $idUser, $idResearch, $time)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryInsert = $connection->prepare("insert into log (hour, action, text, fk_user, fk_research) values (:hour, :action, :text, :fk_user, :fk_research)");

    $queryInsert->bindParam(':hour', $time);
    $queryInsert->bindParam(':action', $actionLog);
    $queryInsert->bindParam(':text', $textLog);
    $queryInsert->bindParam(':fk_user', $idUser);
    $queryInsert->bindParam(':fk_research', $idResearch);

    // Execution des requêtes
    try {
        $queryInsert->execute();
        $reponse = 'ok';
    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }
    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addLogProfil($actionLog, $textLog, $idUser, $time)
{
    $connection = $GLOBALS['connection'];

    $queryInsertResearch = $connection->prepare("insert into research (name, time) values (:name, NOW())");
    $queryInsertResearch->bindParam(':name', $actionLog);

    $queryInsertUser = $connection->prepare("insert into user_research (fk_user, fk_research) values (:fk_user, :fk_research)");
    $queryInsertUser->bindParam(':fk_user', $idUser);

    //Execute Insert Research
    $queryInsertResearch->execute();

    //Execute Insert UserResearch
    $idResearch = $connection->lastInsertId();
    $queryInsertUser->bindParam(':fk_research', $idResearch);
    $queryInsertUser->execute();

    $queryInsert = $connection->prepare("insert into log (hour, action, text, fk_user, fk_research) values (:hour, :action, :text, :fk_user, :fk_research)");
    $queryInsert->bindParam(':hour', $time);
    $queryInsert->bindParam(':action', $actionLog);
    $queryInsert->bindParam(':text', $textLog);
    $queryInsert->bindParam(':fk_user', $idUser);
    $queryInsert->bindParam(':fk_research', $idResearch);

    // Execution des requêtes
    try {
        $queryInsert->execute();
        $reponse = 'ok';
    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }
    $array['reponse'] = $reponse;
    return json_encode($array);
}

function getLog($idUser)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryLog = $connection->prepare("SELECT hour, action, text, pseudo FROM log, user WHERE fk_user = :id AND user.id = log.fk_user AND user.authorize_log = 1 ORDER BY hour DESC");
    $queryLog->bindParam(':id', $idUser);

    try {
        // Get Log
        $queryLog->execute();
        $record = $queryLog->fetchAll();

        $arrayLog = array();

        foreach ($record as $row) {
            $hour = $row['hour'];
            $action = $row['action'];
            $text = $row['text'];
            $pseudo = $row['pseudo'];

            $time = date_create($hour);
            $time = $time->format('d/m/Y  H:i:s');

            $arrayLogRow = array();

            $arrayLogRow["hour"] = $time;
            $arrayLogRow["action"] = $action;
            $arrayLogRow["text"] = $text;
            $arrayLogRow["pseudo"] = $pseudo;

            array_push($arrayLog, $arrayLogRow);
        }
    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }
    return json_encode($arrayLog);
}

function deleteResearch($idResearch)
{
    $connection = $GLOBALS['connection'];

    $queryDeleteResearch = $connection->prepare("DELETE from research WHERE id = :id");
    $queryDeleteResearch->bindParam(':id', $idResearch);

    try {
        $reponse = $queryDeleteResearch->execute();
    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($reponse);
}

function getResearch($idResearch)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryResearch = $connection->prepare("select id, name from research WHERE id = :id");
    $queryResearch->bindParam(':id', $idResearch);

    $queryConcept = $connection->prepare("select id, name from concept WHERE fk_research = :id");
    $queryConcept->bindParam(':id', $idResearch);

    $queryTraduction = $connection->prepare("select name from traduction WHERE fk_concept = :id");

    try {
        // Get Research
        $queryResearch->execute();
        $record = $queryResearch->fetch(PDO::FETCH_OBJ);
        $name = $record->name;
        $array = array("name" => $name);

        // Get Concept
        $queryConcept->execute();
        $record = $queryConcept->fetchAll();

        $arrayConcept = array();

        foreach ($record as $row) {
            $rowArrayConcept = array();
            $id = $row['id'];
            $name = $row['name'];

            array_push($rowArrayConcept, $id);
            array_push($rowArrayConcept, $name);

            // Get Traduction
            $queryTraduction->bindParam(':id', $id);
            $queryTraduction->execute();
            $record2 = $queryTraduction->fetchAll();
            $arrayTraduction = array();

            foreach ($record2 as $row2) {
                $name = $row2['name'];
                array_push($arrayTraduction, $name);
            }
            array_push($rowArrayConcept, $arrayTraduction);
            array_push($arrayConcept, $rowArrayConcept);
        }

        array_push($array, $arrayConcept);

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($array);
}

function addResearchInitial($research, $idUser, $idResearch)
{

    $connection = $GLOBALS['connection'];

    $nameResearch = $research[0];

    // Préparation des requêtes
    $queryCount = $connection->prepare("select count(id) as count from research WHERE id = :id");
    $queryCount->bindParam(':id', $idResearch);

    $queryInsertConcept = $connection->prepare("insert into concept (name, fk_research) values (:name, :fk_research)");

    // Control if the same research exist
    try {
        // Get Research
        $queryCount->execute();
        $recordCount = $queryCount->fetch(PDO::FETCH_OBJ);
        $count = $recordCount->count;

        // Update
        if ($count > 0) {
            $queryUpdate = $connection->prepare("update research
                set
                    name=:name,
                    time=NOW()
                where
                    id=:id");
            $queryUpdate->bindParam(':name', $nameResearch);
            $queryUpdate->bindParam(':id', $idResearch);

            //Execute update research
            $queryUpdate->execute();

            //Update concept
            //Select existing concept
            $querySelectConcept = $connection->prepare("SELECT name from concept WHERE fk_research = :id");
            $querySelectConcept->bindParam(':id', $idResearch);
            $querySelectConcept->execute();
            $recordConcept = $querySelectConcept->fetchAll();

            // concept from Form
            $nameConceptForm = $research[1];

            //cpt concept form
            $cpt = 0;

            // create a array of 4 elements from BDD concept
            while (count($recordConcept) < 4) {
                array_push($recordConcept, array("name" => ""));
            }

            //Loop concept from bdd
            foreach ($recordConcept as $row) {

                //name concept from bdd
                $nameConceptBdd = $row['name'];

                //If concept bdd == concept form
                if ($nameConceptBdd != $nameConceptForm[$cpt]) {
                    if ($nameConceptForm[$cpt] != "") {
                        if ($nameConceptBdd == "") {
                            // Add
                            $queryInsertConcept->bindParam(':fk_research', $idResearch);
                            $queryInsertConcept->bindParam(':name', $nameConceptForm[$cpt]);
                            $queryInsertConcept->execute();
                        } else {
                            // Update
                            $queryUpdate = $connection->prepare("update concept
                            set
                                name=:nameform
                            where
                                fk_research=:fk_research
                            AND name = :namebdd");
                            $queryUpdate->bindParam(':fk_research', $idResearch);
                            $queryUpdate->bindParam(':nameform', $nameConceptForm[$cpt]);
                            $queryUpdate->bindParam(':namebdd', $nameConceptBdd);
                            $queryUpdate->execute();
                        }
                    } else {
                        //Delete
                        $queryDeleteConcept = $connection->prepare("delete from concept where fk_research = :id AND name = :name");
                        $queryDeleteConcept->bindParam(':id', $idResearch);
                        $queryDeleteConcept->bindParam(':name', $nameConceptBdd);
                        $queryDeleteConcept->execute();
                    }
                }
                $cpt++;
            }

        } // Add
        else {
            $queryInsertResearch = $connection->prepare("insert into research (name, time) values (:name, NOW())");
            $queryInsertResearch->bindParam(':name', $nameResearch);

            $queryInsertUser = $connection->prepare("insert into user_research (fk_user, fk_research) values (:fk_user, :fk_research)");
            $queryInsertUser->bindParam(':fk_user', $idUser);

            //Execute Insert Research
            $queryInsertResearch->execute();

            //Execute Insert UserResearch
            $idResearch = $connection->lastInsertId();
            $queryInsertUser->bindParam(':fk_research', $idResearch);
            $queryInsertUser->execute();

            //Insert Concept
            $concept = $research[1];
            $countConcept = count($concept);
            for ($i = 0; $i < $countConcept; $i++) {
                if ($concept[$i] != "") {
                    $queryInsertConcept->bindParam(':fk_research', $idResearch);
                    $queryInsertConcept->bindParam(':name', $concept[$i]);
                    $queryInsertConcept->execute();
                }
            }
        }
        // Get the id
        $reponse = $idResearch;

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !' . $e->getMessage();
    }

    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addResearchFinal($research, $idResearch)
{

    $connection = $GLOBALS['connection'];

    try {
        //Select idConcept
        $querySelectConcept = $connection->prepare("select id from concept where fk_research = :id and name = :name");
        $querySelectConcept->bindParam(':id', $idResearch, PDO::PARAM_INT);

        // Query insert traduction
        $queryInsertTraduction = $connection->prepare("insert into traduction (name, fk_concept) values (:name, :fk_concept)");

        //For each concept
        $concept = $research[1];
        $countConcept = count($concept);
        for ($i = 0; $i < $countConcept; $i += 2) {
            //Get ID concept
            $querySelectConcept->bindParam(':name', $concept[$i]);
            $querySelectConcept->execute();
            $recordId = $querySelectConcept->fetch(PDO::FETCH_OBJ);
            $idConcept = $recordId->id;

            //Delete traduction to the specific concept
            $queryDelete = $connection->prepare("delete from traduction where fk_concept = :id");
            $queryDelete->bindParam(':id', $idConcept, PDO::PARAM_INT);
            $queryDelete->execute();

            //Add traduction to the specific concept
            $traduction = $concept[$i + 1];
            $countTraduction = count($traduction);
            for ($j = 0; $j < $countTraduction; $j++) {
                $queryInsertTraduction->bindParam(':fk_concept', $idConcept);
                $queryInsertTraduction->bindParam(':name', $traduction[$j]);
                $queryInsertTraduction->execute();
            }
        }

        // Get the id
        $reponse = $idResearch;

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !' . $e->getMessage();
    }

    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addExclusion($exclude, $idResearch)
{
    $connection = $GLOBALS['connection'];

    try {
        //Delete Exclude
        $queryDeleteExclude = $connection->prepare("delete from exclusion where fk_research = :id");
        $queryDeleteExclude->bindParam(':id', $idResearch, PDO::PARAM_INT);
        $queryDeleteExclude->execute();

        // Query insert exclude
        $queryInsertExclude = $connection->prepare("insert into exclusion (name, fk_research) values (:name, :fk_research)");
        $queryInsertExclude->bindParam(':fk_research', $idResearch, PDO::PARAM_INT);

        //Add exclude word to the specific research
        $countExclude = count($exclude);
        for ($j = 0; $j < $countExclude; $j++) {
            $queryInsertExclude->bindParam(':name', $exclude[$j]);
            $queryInsertExclude->execute();
        }

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !' . $e->getMessage();
    }

    $array['reponse'] = $reponse;
    return json_encode($array);
}

function addUser($pseudo, $password, $statut, $email)
{

    $connection = $GLOBALS['connection'];

    $salt = $GLOBALS['salt'];
    $criptedPassword = md5($salt . $password);
    $year = date("Y");

    // Préparation des requêtes
    $queryUser = $connection->prepare("select COUNT(*) AS count FROM user WHERE pseudo = :pseudo");
    $queryEmail = $connection->prepare("select COUNT(*) AS count FROM user WHERE email = :email");

    $queryInsert = $connection->prepare("insert into user (pseudo, password, statut, annee, email) values (:pseudo, :password, :statut, :year, :email)");
    $queryInsert->bindParam(':pseudo', $pseudo);
    $queryInsert->bindParam(':password', $criptedPassword);
    $queryInsert->bindParam(':statut', $statut);
    $queryInsert->bindParam(':year', $year);
    $queryInsert->bindParam(':email', $email);

    // Execution des requêtes
    try {
        $queryUser->execute(array(':pseudo' => $pseudo));
        $recordUser = $queryUser->fetch(PDO::FETCH_OBJ);
        $nbUser = $recordUser->count;

        $queryEmail->execute(array(':email' => $email));
        $recordEmail = $queryEmail->fetch(PDO::FETCH_OBJ);
        $nbEmail = $recordEmail->count;

    } catch (Exception $e) {
        echo 'Une erreur est survenue !';
    }

    if (!empty($pseudo) || !empty($password) || !empty($email)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (strlen($password) >= 8) {
                if ($nbUser == 0 && $nbEmail == 0) {
                    try {
                        $queryInsert->execute();

                        $fk_user = $connection->lastInsertId();

                        $reponse = $fk_user;

                    } catch (Exception $e) {
                        $reponse = 'Une erreur est survenue !';
                    }
                } else {
                    $reponse = "Le pseudo ou l'email est déjà utilisé";
                }
            } else {
                $reponse = "Le mot de passe doit contenir au moins 8 caractères";
            }
        } else {
            $reponse = "L'email n'est pas valide";
        }
    } else {
        $reponse = "Tous les champs sont obligatoires";
    }

    $array['reponse'] = $reponse;
    return json_encode($array);

}

function getUser($pseudo, $password)
{

    $connection = $GLOBALS['connection'];
    $salt = $GLOBALS['salt'];

    // Cryptage du mot de passe ou non si mot de passe par défaut
    $criptedPassword = md5($salt . $password);

    // Préparation des requêtes
    $queryCount = $connection->prepare("select count(*) as count from user WHERE pseudo = :pseudo AND password = :password");
    $queryIdUser = $connection->prepare("select id from user WHERE pseudo = :pseudo AND password = :password");

    // Execution des requêtes
    try {
        $queryCount->execute(array(':pseudo' => $pseudo, ':password' => $criptedPassword));
        $queryIdUser->execute(array(':pseudo' => $pseudo, ':password' => $criptedPassword));

        $recordCount = $queryCount->fetch(PDO::FETCH_OBJ);
        $nbUser = $recordCount->count;

    } catch (Exception $e) {
        echo 'Une erreur est survenue !';
    }

    // Vérification login / mot de passe
    if (!empty($pseudo) || !empty($password)) {
        if ($nbUser == 1) {

            try {
                $recordIdUser = $queryIdUser->fetch(PDO::FETCH_OBJ);
                $idUser = $recordIdUser->id;

                // Lance la session
                session_start();

                $_SESSION['pseudo'] = $pseudo;
                $_SESSION['password'] = $criptedPassword;
                $_SESSION['id'] = $idUser;
                $_SESSION['isAdmin'] = md5($salt . $pseudo);
                $reponse = "ok";
            } catch (Exception $e) {
                $reponse = "Une erreur est survenue";
            };

        } else {
            $reponse = "Login ou mot de passe incorrect";
        }
    } else {
        $reponse = "Tous les champs sont obligatoires";
    }

    // Retourne la réponse en JSON
    $array['reponse'] = $reponse;

    return json_encode($array);
}

function sendPassword($pseudo)
{
    $connection = $GLOBALS['connection'];
    $salt = $GLOBALS['salt'];

    // Préparation des requêtes
    $query = $connection->prepare("select id, email from user WHERE pseudo = :pseudo");
    $queryUpdate = $connection->prepare("update user
                set
                    password=:password
                where
                    id=:id");

    // Execution des requêtes
    try {
        $query->execute(array(':pseudo' => $pseudo));
        $record = $query->fetch(PDO::FETCH_OBJ);
        $idUser = $record->id;
        $email = $record->email;
    } catch (Exception $e) {
        echo 'Une erreur est survenue !';
    }

    // Vérification login / mot de passe
    if (!empty($email)) {
        if (!empty($idUser)) {
            $newPassword = randomPassword();
            $reponse = true;
            // Cryptage du mot de passe ou non si mot de passe par défaut
            $criptedPassword = md5($salt . $newPassword);
            $queryUpdate->bindParam(':password', $criptedPassword);
            $queryUpdate->bindParam(':id', $idUser);
            $queryUpdate->execute();
            mail($email, 'MALIN - Mot de passe réinitialisé', "Votre nouveau mot de passe est " . $newPassword);
        } else {
            $reponse = "Email incorrect";
        }
    } else {
        $reponse = "Il n'y a pas d'email associé à ce pseudo";
    }

    // Retourne la réponse en JSON
    $array['reponse'] = $reponse;

    return json_encode($array);
}

function getResearchList($idUser)
{

    $connection = $GLOBALS['connection'];
    $name = "Utilisateur";

    // Préparation des requêtes
    $queryList = $connection->prepare("select id, name, time from research r, user_research ur WHERE r.id = ur.fk_research AND ur.fk_user = :id AND r.name NOT LIKE :name");
    $queryList->bindParam(':id', $idUser);
    $queryList->bindParam(':name', $name);

    try {
        $queryList->execute();
        $record = $queryList->fetchAll();
        $array = array();
        foreach ($record as $row) {
            $rowArray = array();

            $id = $row['id'];
            $name = $row['name'];
            $time = $row['time'];

            $time = date_create($time);
            $time = $time->format('d/m/Y à H:i:s');

            array_push($rowArray, $id);
            array_push($rowArray, $name);
            array_push($rowArray, $time);

            array_push($array, $rowArray);
        }

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($array);
}


function getUserList($time, $statut)
{

    $connection = $GLOBALS['connection'];

    $idAdmin = 1;

    // Préparation des requêtes
    $queryList = $connection->prepare("select  distinct id,  pseudo from user u, user_research ur WHERE u.id NOT LIKE :id AND u.id = ur.fk_user AND u.annee LIKE :time AND u.statut LIKE :statut ORDER BY pseudo ASC");
    $queryList->bindParam(':id', $idAdmin, PDO::PARAM_INT);
    $queryList->bindParam(':time', $time);
    $queryList->bindParam(':statut', $statut);

    try {
        $queryList->execute();
        $record = $queryList->fetchAll();
        $array = array();
        foreach ($record as $row) {
            $rowArray = array();

            $id = $row['id'];
            $name = $row['pseudo'];

            array_push($rowArray, $id);
            array_push($rowArray, $name);

            array_push($array, $rowArray);
        }

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($array);
}

function getYearList()
{

    $connection = $GLOBALS['connection'];
    // Préparation des requêtes
    $queryList = $connection->prepare("select distinct  annee from user u ORDER BY annee DESC");

    try {
        $queryList->execute();
        $record = $queryList->fetchAll();
        $array = array();
        foreach ($record as $row) {
            $rowArray = array();

            $year = $row['annee'];

            array_push($rowArray, $year);

            array_push($array, $rowArray);
        }

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($array);
}

function getStatutList($year)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryList = $connection->prepare("select distinct  statut from user u WHERE u.annee = :year ORDER BY statut ASC");
    $queryList->bindParam(':year', $year);

    try {
        $queryList->execute();
        $record = $queryList->fetchAll();
        $array = array();
        foreach ($record as $row) {
            $rowArray = array();

            $statut = $row['statut'];

            array_push($rowArray, $statut);

            array_push($array, $rowArray);
        }

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    return json_encode($array);
}

function getUserProfil($idUser)
{

    $connection = $GLOBALS['connection'];

    // Préparation des requêtes
    $queryUser = $connection->prepare("select id, pseudo, authorize_log, statut, email from user WHERE id = :idUser");
    $queryUser->bindParam(':idUser', $idUser);

    try {
        $queryUser->execute();
        $record = $queryUser->fetch(PDO::FETCH_OBJ);
        $name = $record->pseudo;
        $statut = $record->statut;
        $email = $record->email;
        $password = 'xxxxxxxx';
        $authorizelog = $record->authorize_log;

        $array = array("name" => $name, "password" => $password, "authorize_log" => $authorizelog, "statut" => $statut, "email" => $email);

    } catch (Exception $e) {
        $reponse = 'Une erreur est survenue !';
    }

    echo json_encode($array);
}

function updateUserProfil($idUser, $pseudo, $password, $log, $statut, $email)
{

    $connection = $GLOBALS['connection'];
    $salt = $GLOBALS['salt'];

    // Cryptage du mot de passe ou non si mot de passe par défaut
    $criptedPassword = md5($salt . $password);

    // Count
    $queryUser = $connection->prepare("select COUNT(*) AS count FROM user WHERE pseudo = :pseudo AND id != :id");
    $queryUser->bindParam(':pseudo', $pseudo);
    $queryUser->bindParam(':id', $idUser);
    $queryUser->execute();
    $recordCount = $queryUser->fetch(PDO::FETCH_OBJ);
    $nbUser = $recordCount->count;

    $queryEmail = $connection->prepare("select COUNT(*) AS count FROM user WHERE email LIKE :email AND id != :id");
    $queryEmail->bindParam(':email', $email);
    $queryEmail->bindParam(':id', $idUser);
    $queryEmail->execute();
    $recordEmail = $queryEmail->fetch(PDO::FETCH_OBJ);
    $nbEmail = $recordEmail->count;

    // Update de l'utilisateur
    $queryUpdate = $connection->prepare("update user
                set 
                    pseudo=:pseudo,
					password=:password,
					authorize_log=:log,
					statut=:statut,
					email=:email
                where
                    id=:id");
    $queryUpdateNoPassword = $connection->prepare("update user
                set 
                    pseudo=:pseudo,
                    authorize_log=:log,
                    statut=:statut,
                    email=:email
                where
                    id=:id");

    $queryUpdate->bindParam(':pseudo', $pseudo);
    $queryUpdate->bindParam(':password', $criptedPassword);
    $queryUpdate->bindParam(':id', $idUser);
    $queryUpdate->bindParam(':log', $log);
    $queryUpdate->bindParam(':statut', $statut);
    $queryUpdate->bindParam(':email', $email);

    $queryUpdateNoPassword->bindParam(':pseudo', $pseudo);
    $queryUpdateNoPassword->bindParam(':id', $idUser);
    $queryUpdateNoPassword->bindParam(':log', $log);
    $queryUpdateNoPassword->bindParam(':statut', $statut);
    $queryUpdateNoPassword->bindParam(':email', $email);

    if (!empty($pseudo) || !empty($password) || !empty($email)) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (strlen($password) >= 8) {
                if ($nbUser < 1 && $nbEmail < 1) {
                    // Si il ne change pas le password on l'update pas car par défaut c'est noté xxxxxxxx
                    if ($password != 'xxxxxxxx') {
                        try {
                            $queryUpdate->execute();
                            $reponse = 'ok';

                        } catch (Exception $e) {
                            $reponse = 'Erreur de requète : ' . $e->getMessage();
                        }

                    } else {
                        try {
                            $queryUpdateNoPassword->execute();
                            $reponse = 'ok';

                        } catch (Exception $e) {
                            $reponse = 'Erreur de requète : ' . $e->getMessage();
                        }

                    }
                } else {
                    $reponse = "Le pseudo ou l'email est déjà utilisé";
                }

            } else {
                $reponse = "Le mot de passe doit contenir au moins 8 caractères";
            }
        } else {
            $reponse = "L'email n'est pas valide";
        }
    } else {
        $reponse = "Tous les champs sont obligatoires";
    }
    $array['reponse'] = $reponse;
    return json_encode($array);
}

function randomPassword()
{
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

?>